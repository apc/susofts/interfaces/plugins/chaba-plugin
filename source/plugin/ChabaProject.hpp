/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPROJECT_HPP
#define CHABAPROJECT_HPP

#include <string>
#include <unordered_set>

/**
 * Class representing a Chaba project.
 *
 * It contains all the information from a Chaba project file.
 */
class ChabaProject
{
public:
	struct Param
	{
		bool variable = true;
		double value = 0;
		bool operator==(const Param &o) const noexcept { return variable == o.variable && value == o.value; }
		bool operator!=(const Param &o) const noexcept { return !(*this == o); }
	};

public:
	/** title */
	std::string title = "dummy";
	/** precision */
	size_t precision = 5;
	/** tolerance */
	double tolerance = 0.005;
	/** maxIterations */
	unsigned int maxIterations = 50;
	/** scale */
	Param scale{false, 1.};
	/** translation in X */
	Param trax;
	/** translation Y */
	Param tray;
	/** translation Z */
	Param traz;
	/** rotation X */
	Param rotx;
	/** rotation Y */
	Param roty;
	/** roatation Z */
	Param rotz;
	/** full path to project file */
	std::string projectPath;
	/** passive input file */
	std::string passiveFile;
	/** active input file */
	std::string activeFile;
	/** coo output file */
	std::string cooFile;
	/** punch output file */
	std::string punchFile;
	/** result output file */
	std::string outFile;
	// extra
	/** active (reference) points */
	std::unordered_set<std::string> activePoints;
	/** passive points */
	std::unordered_set<std::string> passivePoints;

public:
	bool operator==(const ChabaProject &o) const noexcept
	{
		return title == o.title && precision == o.precision && tolerance == o.tolerance && maxIterations && o.maxIterations && scale == o.scale && trax == o.trax
			&& tray == o.tray && traz == o.traz && rotx == o.rotx && roty == o.roty && rotz == o.rotz && projectPath == o.projectPath && passiveFile == o.passiveFile
			&& activeFile == o.activeFile && cooFile == o.cooFile && punchFile == o.punchFile && outFile == o.outFile && activePoints == o.activePoints
			&& passivePoints == o.passivePoints;
	}
	bool operator!=(const ChabaProject &o) const noexcept { return !(*this == o); }
};

#endif // CHABAPROJECT_HPP
