/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPOINTTABLEEDITOR_HPP
#define CHABAPOINTTABLEEDITOR_HPP

#include <editors/table/TableEditor.hpp>

class ChabaPlugin;

class ChabaPointTableEditor : public TableEditor
{
public:
	/**
	 * Constructor for ChabaPointTableEditor.
	 *
	 * @param plugin ChabaPlugin pointer passed to the constructor cannot be a nullptr.
	 * @param activeMode Determines if the ChabaPointTableEditor should operate on active or passive points.
	 * @param parent QWidget parent.
	 *
	 * @see TableEditor, ChabaPointGui, TableEditorWidget
	 */
	ChabaPointTableEditor(ChabaPlugin *plugin, bool activeMode = true, QWidget *parent = nullptr);
	virtual ~ChabaPointTableEditor() override = default;

	virtual void paste() override;
	virtual void copy() override;
	virtual QString getRowAnnotation(const QItemSelection &selected) const override;
	virtual void precision(int precision) override;
	virtual int precision() override;

public slots:
	/**
	 * Sets the relevant color coding for the rows in the table:
	 * - point deactivated = gray color,
	 * - point name repeats = red color,
	 * - point is a reference = green color,
	 * - none of the above = default (white) color.
	 */
	virtual void updateRowStatus(QTableWidgetItem *item = nullptr) override;

protected:
	virtual ShareablePoint parseRow(int row) const override;
	virtual void setPoint(int row, const ShareablePoint &point) override;

private:
	/** Sets the desired background color for a given row. */
	void setRowColor(int row, QColor color);

	/** pimpl */
	class _ChabaPointTableEditor_pimpl;
	std::unique_ptr<_ChabaPointTableEditor_pimpl> _pimpl;
};

#endif // CHABAPOINTTABLEEDITOR_HPP
