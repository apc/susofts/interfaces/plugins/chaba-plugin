#include "ChabaPointGui.hpp"

#include <QBoxLayout>
#include <QComboBox>
#include <QLabel>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <ShareablePoints/IShareablePointsListIO.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/table/CommandTable.hpp>
#include <editors/table/TableEditor.hpp>
#include <utils/UndoStack.hpp>

#include "ChabaPlugin.hpp"
#include "ChabaProject.hpp"
#include "tabs/ChabaCooLexer.hpp"
#include "tabs/inputs/ChabaPointLexerIO.hpp"
#include "tabs/inputs/ChabaPointTableEditor.hpp"

namespace
{
/**
 * CommandComboBox is an implementation of IUndoCommand for handling undo/redo for QComboBox.
 *
 * @see IUndoCommand
 */
class CommandComboBox : public IUndoCommand
{
public:
	CommandComboBox(QComboBox *cb) : IUndoCommand(), _comboBox(cb), _referential(_comboBox->currentText()) {}

	// UndoCommand
	virtual void restoreState() override
	{
		_comboBox->blockSignals(true);
		_comboBox->setCurrentText(_referential);
		_comboBox->blockSignals(false);
	}

private:
	QComboBox *_comboBox;
	QString _referential;
};

size_t _getPrecision(const SPluginInterface *plugin, const QWidget *currentWidget)
{
	const auto *p = dynamic_cast<const ChabaPlugin *>(plugin);
	const ChabaProject *project = p ? p->_getProject(currentWidget) : nullptr;
	if (project)
		return project->precision;
	return 5;
}
} // namespace

class ChabaPointGui::_ChabaPointGui_pimpl
{
public:
	bool activeMode = true;
	bool cooMode = false;
	QComboBox *combo = nullptr;
	UndoStack undoStack;
};

ChabaPointGui::ChabaPointGui(TableEditor *editor, SPluginInterface *owner, bool activeMode, QWidget *parent) :
	TableEditorWidget(editor, owner, parent), _pimpl(std::make_unique<_ChabaPointGui_pimpl>())
{
	QWidget *widget = new QWidget(this);
	QVBoxLayout *layout = new QVBoxLayout;
	QLabel *label = new QLabel(tr("Referential"), this);
	QComboBox *combo = new QComboBox;
	combo->addItem("*OLOC");
	combo->addItem("*RS2K");
	combo->addItem("*LEP");
	combo->addItem("*SPHE");
	_pimpl->combo = combo;
	_pimpl->activeMode = activeMode;

	layout->addWidget(label);
	layout->addWidget(combo);
	layout->addWidget(editor);
	widget->setLayout(layout);
	setWidget(widget);

	// connects
	// undo/redo
	connect(&_pimpl->undoStack, &UndoStack::hasChanged, [this](bool isUndo, bool isRedo) {
		emit undoAvailable(isUndo);
		emit redoAvailable(isRedo);
	});
	// If table modified
	connect(this->editor(), &TableEditor::actionDone, &_pimpl->undoStack, &UndoStack::push);
	// If comboBox text changed
	connect(_pimpl->combo, &QComboBox::currentTextChanged, [this]() {
		isModified(true);
		_pimpl->undoStack.push(new CommandComboBox(_pimpl->combo));
	});
}

ShareablePointsList ChabaPointGui::getContent() const
{
	auto spl = editor()->getContent();
	bool _isOLOC = (_pimpl->combo->currentText() == "*OLOC");
	spl.getParams() = ShareableParams{getPrecision(), _isOLOC ? ShareableParams::ECoordSys::k3DCartesian : ShareableParams::ECoordSys::k2DPlusH,
		{{"coordinateSystem", _pimpl->combo->currentText().toStdString()}}};
	return spl;
}

void ChabaPointGui::setContent(const ShareablePointsList &spl)
{
	setPrecision((int)_getPrecision(owner(), this));
	TableEditorWidget::setContent(spl);
	if (spl.getParams().extraInfos.has("coordinateSystem"))
		_pimpl->combo->setCurrentText(QString::fromStdString(spl.getParams().extraInfos.at("coordinateSystem")));
	_pimpl->undoStack.reset({new CommandComboBox(_pimpl->combo), new CommandTable(editor())});
}

void ChabaPointGui::undo()
{
	TableEditorWidget::undo();
	_pimpl->undoStack.undo();
}

void ChabaPointGui::redo()
{
	TableEditorWidget::redo();
	_pimpl->undoStack.redo();
}

bool ChabaPointGui::_save(const QString &path)
{
	if (path.isEmpty())
		return false;
	if (_pimpl->cooMode)
		IShareablePointsListIO::writeFile(path.toStdString(), ChabaCooLexer().write(getContent()));
	else
	{
		ChabaPointLexerIO lexer;
		size_t precision = _getPrecision(owner(), this);
		lexer.setPrecision(precision);
		IShareablePointsListIO::writeFile(path.toStdString(), lexer.write(getContent()));
		setPrecision((int)precision);
	}
	return true;
}

bool ChabaPointGui::_open(const QString &path)
{
	if (path.isEmpty())
		return false;
	try
	{
		std::string text = IShareablePointsListIO::readFile(path.toStdString());
		_pimpl->cooMode = text[0] == '#';
		if (_pimpl->cooMode)
			setContent(ChabaCooLexer().read(text));
		else
			setContent(ChabaPointLexerIO().read(text));
	}
	catch (const SPIOException &e)
	{
		logDebug() << "ChabaPointGui: open file, " << e.what();
		return false;
	}
	return true;
}

void ChabaPointGui::_newEmpty()
{
	setContent(_pimpl->activeMode ? ChabaPlugin::_activePointsTemplate() : ChabaPlugin::_passivePointsTemplate());
}
