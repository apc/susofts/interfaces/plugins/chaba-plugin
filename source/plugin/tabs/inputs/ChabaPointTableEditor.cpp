#include "ChabaPointTableEditor.hpp"

#include <cmath>
#include <limits>

#include <QMimeData>

#include <Logger.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <editors/table/CommandTable.hpp>
#include <editors/table/TableDoubleDelegate.hpp>
#include <editors/table/TableStringDelegate.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/StylesHelper.hpp>

#include "ChabaPlugin.hpp"
#include "ChabaProject.hpp"
#include "tabs/inputs/ChabaPointLexerIO.hpp"

class ChabaPointTableEditor::_ChabaPointTableEditor_pimpl
{
public:
	ChabaPlugin *plugin;
	bool activeMode = true;
};

ChabaPointTableEditor::ChabaPointTableEditor(ChabaPlugin *plugin, bool activeMode, QWidget *parent) :
	TableEditor(parent), _pimpl(std::make_unique<_ChabaPointTableEditor_pimpl>())
{
	_pimpl->activeMode = activeMode;
	_pimpl->plugin = plugin;

	std::vector<QString> headers{"active", "name",
		// position
		"x", "y", "z", "sigmax", "sigmay", "sigmaz",
		// comments
		"DCUM", "ID", "Comment"};
	setColumnCount((int)headers.size());
	for (unsigned int i = 0; i < headers.size(); i++)
		setHorizontalHeaderItem(i, createItem(headers[i]));

	// Set a delegate for point name
	setItemDelegateForColumn(1, new TableStringDelegate(this, "^[a-zA-Z0-9_]+$"));
	// Set double delegates
	setItemDelegateForColumn(2, new TableDoubleDelegate(this));
	setItemDelegateForColumn(3, new TableDoubleDelegate(this));
	setItemDelegateForColumn(4, new TableDoubleDelegate(this));
	setItemDelegateForColumn(5, new TableDoubleDelegate(this));
	setItemDelegateForColumn(6, new TableDoubleDelegate(this));
	setItemDelegateForColumn(7, new TableDoubleDelegate(this));
}

void ChabaPointTableEditor::paste()
{
	ShareablePointsList spl;
	try
	{
		ChabaPointLexerIO io;
		auto content = ClipboardManager::getClipboardManager().getFromClipboard(QString::fromStdString(io.getMIMEType()));
		spl = ClipboardManager::getClipboardManager().serializeFromString(content.toStdString(), &io);
	}
	catch (const std::exception &)
	{
		try
		{
			spl = ClipboardManager::getClipboardManager().paste();
		}
		catch (const std::exception &e)
		{
			logInfo() << "Unable to paste,'" << e.what() << "'";
			return;
		}
	}
	auto points = spl.getRootFrame().getAllPoints();
	if (!selectionModel()->selectedRows().isEmpty()) // if row selected - overwrite selected row and the ones below
	{
		for (unsigned int i = 0; i < points.size(); i++)
			setPoint(selectionModel()->selectedRows()[0].row() + i, *points[i]);
	}
	else if (!selectedItems().isEmpty()) // if only table item(s) selected - insert new points below
	{
		for (unsigned int i = 0; i < points.size(); i++)
		{
			insertRow(selectedItems().last()->row() + 1 + i);
			setPoint(selectedItems().last()->row() + 1 + i, *points[i]);
		}
	}
	else // if no selection - append to the end of table
	{
		for (auto point : points)
			setPoint(rowCount(), *point);
	}
	emit actionDone(new CommandTable(this));
}

void ChabaPointTableEditor::copy()
{
	auto selectionToText = [this](char separator) -> QByteArray {
		QByteArray result;
		auto items = selectedItems();
		for (int i = 0; i < items.count(); i++)
		{
			result += items.at(i)->text().toUtf8();
			if (i + 1 < items.count() && (items.at(i)->row() != items.at(i + 1)->row()))
				result += '\n';
			else
				result += separator;
		}
		return result.left(result.length() - 1); // do not use the last separator
	};

	auto &clipboard = ClipboardManager::getClipboardManager();
	ShareablePointsList spl;
	ShareableFrame &frame = spl.getRootFrame();
	auto mimedata = new QMimeData();

	try
	{
		if (selectionModel()->selectedRows().isEmpty())
			throw SPIOException("Only part of the row(s) was chosen.");
		for (auto &rowId : selectionModel()->selectedRows())
			frame.add(new ShareablePoint(parseRow(rowId.row())));
		clipboard.copy(spl, mimedata);
	}
	catch (const std::exception &e)
	{
		logInfo() << "Impossible to serialize the selection for the copy: '" << e.what() << "'";
		mimedata->setData("text/csv", selectionToText(','));
	}
	ChabaPointLexerIO io;
	mimedata->setData(QString::fromStdString(io.getMIMEType()), io.write(spl).c_str());
	mimedata->setData("text/plain", selectionToText('\t'));
	ClipboardManager::getClipboardManager().putInClipboard(mimedata);
}

QString ChabaPointTableEditor::getRowAnnotation(const QItemSelection &selected) const
{
	// if no selection
	if (selected.indexes().isEmpty())
		return ChabaPlugin::_HELP_STRING.arg(_pimpl->activeMode ? tr("Active (reference)") : tr("Passive"));
	auto type = item(selected.indexes()[0].row(), 0)->background().color();
	// deactivated
	if (type == QColor(195, 195, 195, 75))
		return ChabaPlugin::_POINTDEAC_STRING + ChabaPlugin::_POINT_STRING;
	// name repeats
	else if (type == QColor(230, 185, 185, 75))
		return ChabaPlugin::_POINTINVALID_STRING + ChabaPlugin::_POINT_STRING;
	// reference
	else if (type == QColor(185, 230, 185, 75))
		return ChabaPlugin::_POINTREF_STRING + ChabaPlugin::_POINT_STRING;
	// normal point
	else if (type == QColor(255, 255, 255))
		return ChabaPlugin::_POINT_STRING;
	else
		return ChabaPlugin::_HELP_STRING.arg(_pimpl->activeMode ? tr("Active (reference)") : tr("Passive"));
}

void ChabaPointTableEditor::precision(int precision)
{
	// for TableDoubleDelegate
	for (int col = 2; col <= 7; col++)
	{
		auto columnDelegate = qobject_cast<TableDoubleDelegate *>(itemDelegateForColumn(col));
		if (columnDelegate)
			columnDelegate->precision(precision);
	}
}

int ChabaPointTableEditor::precision()
{
	auto columnDelegate = qobject_cast<TableDoubleDelegate *>(itemDelegateForColumn(1));
	if (columnDelegate)
		return columnDelegate->precision();
	return -1; // no TableDoubleDelegate
}

void ChabaPointTableEditor::updateRowStatus(QTableWidgetItem *)
{
	auto *project = _pimpl->plugin->_getProject(this);
	auto &toinsert = _pimpl->activeMode ? project->activePoints : project->passivePoints;
	const auto &tocheck = _pimpl->activeMode ? project->passivePoints : project->activePoints;
	bool DDmode = true;
	toinsert.clear();

	// in order of priority
	blockSignals(true);
	for (int r = 0; r < rowCount(); r++)
	{
		// if deactivated
		if (item(r, 0) && !item(r, 0)->data(0).toBool())
			setRowColor(r, QColor(195, 195, 195, 75));
		else
		{
			const auto &[it, dontexist] = toinsert.insert(item(r, 1)->data(0).toString().toStdString());
			// if name repeats
			if (item(r, 1) && !dontexist)
				setRowColor(r, QColor(230, 185, 185, 75));
			// if a reference
			else if (item(r, 1) && tocheck.find(item(r, 1)->data(0).toString().toStdString()) != std::cend(tocheck))
				setRowColor(r, QColor(185, 230, 185, 75));
			// if none - default look
			else
				setRowColor(r, getThemeColor(Qt::white));
		}
		if (item(r, 4) && !std::isnan(item(r, 4)->data(0).toDouble()))
			DDmode = false;
	}
	blockSignals(false);
	setColumnHidden(4, DDmode);
	setColumnHidden(7, DDmode);
}

ShareablePoint ChabaPointTableEditor::parseRow(int row) const
{
	ShareablePoint sp;
	sp.active = item(row, 0)->text() == "true";
	sp.name = item(row, 1)->text().toStdString();
	if (sp.name.empty())
		throw SPIOException("Can't convert a row with empty name to a ShareablePoint.");
	// Position
	sp.position = ShareablePosition{item(row, 2)->text().toDouble(), item(row, 3)->text().toDouble(),
		!isColumnHidden(4) && std::isnan(item(row, 4)->data(0).toDouble()) ? 0 : item(row, 4)->text().toDouble(), item(row, 5)->text().toDouble(),
		item(row, 6)->text().toDouble(), item(row, 7)->text().toDouble()};
	// Extra infos
	if (item(row, 8) && !item(row, 8)->text().isEmpty())
		sp.extraInfos.addExtraInfo("DCUM", item(row, 8)->text().toStdString());
	if (item(row, 9) && !item(row, 9)->text().isEmpty())
		sp.extraInfos.addExtraInfo("id", item(row, 9)->text().toStdString());
	// Comments
	sp.inlineComment = item(row, 10)->text().toStdString();
	return sp;
}

void ChabaPointTableEditor::setPoint(int row, const ShareablePoint &point)
{
	if (rowCount() <= row)
		insertRow(row);

	blockSignals(true);
	setItem(row, 0, createItem(point.active));
	// Name
	setItem(row, 1, createItem(QString::fromStdString(point.name)));
	// Position
	setItem(row, 2, createItem(point.position.x));
	setItem(row, 3, createItem(point.position.y));
	setItem(row, 4, createItem(point.position.z));
	setItem(row, 5, createItem(point.position.sigmax));
	setItem(row, 6, createItem(point.position.sigmay));
	setItem(row, 7, createItem(point.position.sigmaz));
	// Extra info
	point.extraInfos.has("DCUM") ? setItem(row, 8, createItem(QString::fromStdString(point.extraInfos.at("DCUM")))) : setItem(row, 8, createItem(""));
	point.extraInfos.has("id") ? setItem(row, 9, createItem(QString::fromStdString(point.extraInfos.at("id")))) : setItem(row, 9, createItem(""));
	setItem(row, 10, createItem(QString::fromStdString(point.inlineComment)));
	blockSignals(false);
}

void ChabaPointTableEditor::setRowColor(int row, QColor color)
{
	for (int col = 0; col < columnCount(); col++)
		item(row, col)->setBackground(color);
}
