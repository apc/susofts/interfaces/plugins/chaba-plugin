/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPOINTGUI_HPP
#define CHABAPOINTGUI_HPP

#include <editors/table/TableEditorWidget.hpp>

class TableEditor;

class ChabaPointGui : public TableEditorWidget
{
public:
	ChabaPointGui(TableEditor *editor, SPluginInterface *owner, bool activeMode, QWidget *parent = nullptr);
	virtual ~ChabaPointGui() override = default;

	// SGraphicalWidget
	virtual ShareablePointsList getContent() const override;
	virtual void setContent(const ShareablePointsList &spl) override;

public slots:
	// SGraphicalWidget
	virtual void undo() override;
	virtual void redo() override;

protected:
	// SGraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

private:
	/** pimpl */
	class _ChabaPointGui_pimpl;
	std::unique_ptr<_ChabaPointGui_pimpl> _pimpl;
};

#endif // CHABAPOINTGUI_HPP
