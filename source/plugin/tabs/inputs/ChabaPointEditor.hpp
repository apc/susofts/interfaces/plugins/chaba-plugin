/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPOINTEDITOR_HPP
#define CHABAPOINTEDITOR_HPP

#include <interface/STabInterface.hpp>

struct ChabaMeasures;

class ChabaPointEditor : public STabInterface
{
	Q_OBJECT

public:
	ChabaPointEditor(SPluginInterface *owner, bool activeMode, QWidget *parent = nullptr);
	virtual ~ChabaPointEditor() override;

	// ChabaPointEditor
	const ChabaMeasures &chabaMeasures();

public slots:
	void globalIndicatorClicked(const QString &text, int position);

private:
	/** pimpl */
	class _ChabaPointEditor_pimpl;
	std::unique_ptr<_ChabaPointEditor_pimpl> _pimpl;
};

#endif // CHABAPOINTEDITOR_HPP
