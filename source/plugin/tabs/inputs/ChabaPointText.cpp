#include "ChabaPointText.hpp"

#include <fstream>
#include <iomanip>
#include <sstream>

#include <QContextMenuEvent>
#include <QEvent>
#include <QMenu>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/MarkerViewer.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <utils/SettingsManager.hpp>

#include "ChabaMeasures.hpp"
#include "ChabaPlugin.hpp"
#include "ChabaProject.hpp"
#include "tabs/ChabaCooLexer.hpp"
#include "tabs/inputs/ChabaPointLexerIO.hpp"
#include "trinity/ChabaConfig.hpp"

ChabaPointText::ChabaPointText(SPluginInterface *owner, bool activeMode, QWidget *parent) : TextEditorWidget(owner, parent), _activeMode(activeMode)
{
	updateUi(owner->name());

	// setup editor
	editor()->setMimeTypesSupport(
		{ChabaPointLexerIO().getMIMEType()}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
	// reference marker
	editor()->markerDefine(QPixmap(":/markers/reference").scaled(14, 14), ChabaPointLexerIO::Markers::Reference);
	std::bitset<32> marginmask = editor()->marginMarkerMask(TextMargin::MARKERREPORT);
	marginmask[ChabaPointLexerIO::Markers::Reference] = true;
	editor()->setMarginMarkerMask(TextMargin::MARKERREPORT, (int)marginmask.to_ulong());
	auto markers = markerDefinitions;
	markers[ChabaPointLexerIO::Markers::Reference] = {tr("Reference"), ":/markers/reference"};
	markerViewer()->updateFilters(std::move(markers));

	// actions
	// align contents
	auto *actAlig = new QAction(QIcon(":/actions/align-contents"), tr("Reformat file"), this);
	actAlig->setShortcut(tr("Ctrl+Shift+F"));
	actAlig->setToolTip(tr("Reformat the file and align all the columns"));
	actAlig->setStatusTip(tr("Reformat the file and align all the columns"));
	connect(actAlig, &QAction::triggered, [this]() { alignContents(editor()); });
	addAction(Menus::edit, actAlig);
	// toggle comment
	auto *actToggleComment = new QAction(tr("Toggle comment"), this);
	actToggleComment->setShortcut(tr("Ctrl+Shift+C"));
	actToggleComment->setToolTip(tr("Comment lines"));
	actToggleComment->setStatusTip(tr("Comment lines"));
	connect(actToggleComment, &QAction::triggered, [this]() { toggleKeyword("%"); });
	addAction(Menus::edit, actToggleComment);
	// toggle active
	auto *actToggleActivePoint = new QAction(tr("Toggle active point"), this);
	actToggleActivePoint->setShortcut(tr("Ctrl+Shift+B"));
	actToggleActivePoint->setToolTip(tr("Toggle current line (or selected lines) into (de)activated points"));
	actToggleActivePoint->setStatusTip(tr("Toggle current line (or selected lines) into (de)activated points"));
	connect(actToggleActivePoint, &QAction::triggered, [this]() { toggleKeyword("!/?"); });
	addAction(Menus::edit, actToggleActivePoint);

	additionalContextualActions({actToggleComment, actToggleActivePoint, actAlig});
}

ShareablePointsList ChabaPointText::getContent() const
{
	ShareablePointsList spl;
	if (_cooMode)
		spl = ChabaCooLexer().read(toByteArray(editor()->text(), editor()).constData());
	else
		spl = ChabaPointLexerIO().read(toByteArray(editor()->text(), editor()).constData());
	spl.getParams().precision = (int)getPrecision();
	return spl;
}

const ChabaMeasures &ChabaPointText::chabaMeasures()
{
	static const ChabaMeasures _m;
	auto lex = qobject_cast<ChabaPointLexerIO *>(editor()->lexer());
	return lex ? lex->chabaMeasures() : _m;
}

void ChabaPointText::setContent(const ShareablePointsList &spl)
{
	if (_cooMode)
		editor()->setText(fromByteArray(ChabaCooLexer().write(spl).c_str(), editor()));
	else
	{
		auto lexer = ChabaPointLexerIO();
		lexer.setPrecision(getPrecision());
		editor()->setText(fromByteArray(lexer.write(spl).c_str(), editor()));
	}
}

void ChabaPointText::updateContent()
{
	auto *lexer = qobject_cast<ChabaPointLexerIO *>(editor()->lexer());
	if (lexer)
		lexer->setPrecision(getPrecision());
	TextEditorWidget::updateContent();
}

void ChabaPointText::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);
	if (pluginName != ChabaPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<ChabaConfigObject>(pluginName);

	if (!config.color)
		editor()->setLexer(nullptr);
	else if (!_cooMode && !qobject_cast<ChabaPointLexerIO *>(editor()->lexer()))
	{
		auto *lexer = new ChabaPointLexerIO(this, dynamic_cast<ChabaPlugin *>(owner()));
		lexer->setPrecision(getPrecision());
		lexer->activeMode(_activeMode);
		editor()->setLexer(lexer);
	}
	else if (_cooMode && !qobject_cast<ChabaCooLexer *>(editor()->lexer()))
	{
		auto *lexer = new ChabaCooLexer(this, dynamic_cast<ChabaPlugin *>(owner()));
		lexer->type(_activeMode ? ChabaCooLexer::Type::Active : ChabaCooLexer::Type::Passive);
		editor()->setLexer(lexer);
	}
}

void ChabaPointText::globalIndicatorClicked(const QString &, int identifier)
{
	TextEditor *editor = this->editor();
	if (!editor)
		return;

	const ChabaMeasures &measures = chabaMeasures();
	if (!measures.points.count(identifier))
		return;

	editor->jumpTo(measures.points.at(identifier).position, false);
}

bool ChabaPointText::_open(const QString &path)
{
	bool ret = TextEditorWidget::_open(path);
	_cooMode = editor()->text()[0] == '#';
	updateUi(owner()->name());
	return ret;
}

void ChabaPointText::_newEmpty()
{
	setContent(_activeMode ? ChabaPlugin::_activePointsTemplate() : ChabaPlugin::_passivePointsTemplate());
}

QByteArray ChabaPointText::fromMime(const QMimeData *mimedata)
{
	if (!_cooMode)
	{
		ChabaPointLexerIO io;
		io.utf8(editor()->isUtf8());
		return contentFromMime(mimedata, io);
	}
	ChabaCooLexer io;
	io.utf8(editor()->isUtf8());
	return contentFromMime(mimedata, io);
}

QMimeData *ChabaPointText::toMime(const QByteArray &text, QMimeData *mimedata)
{
	if (!_cooMode)
	{
		ChabaPointLexerIO io;
		io.utf8(editor()->isUtf8());
		return contentToMime(text, mimedata, io);
	}
	ChabaCooLexer io;
	io.utf8(editor()->isUtf8());
	return contentToMime(text, mimedata, io);
}

size_t ChabaPointText::getPrecision() const
{
	const auto *plugin = dynamic_cast<const ChabaPlugin *>(owner());
	const ChabaProject *project = plugin ? plugin->_getProject(this) : nullptr;
	if (project)
		return project->precision;
	return 5;
}

void ChabaPointText::toggleKeyword(const QString &keyword)
{
	Lines l = initEditingLines(editor());
	QStringList keywords = keyword.split('/');

	// toggle comments
	QString linetxt;
	QStringRef lineref;
	editor()->SendScintilla(QsciScintilla::SCI_BEGINUNDOACTION);
	for (int curline = l.endLine; curline >= l.startLine && curline >= 0; curline--)
	{
		linetxt = editor()->text(curline);
		lineref = &linetxt;
		lineref = lineref.trimmed();
		if (lineref.isEmpty())
			continue;

		int commentOffset = (int)(lineref[0] == '%' && keywords[0] != '%'); // if the first char is comment and we don't treat comments
		if (keywords.contains(lineref[0 + commentOffset])) // remove keyword
		{
			int length = 1;
			editor()->SendScintilla(QsciScintilla::SCI_DELETERANGE, editor()->positionFromLineIndex(curline, lineref.position() + commentOffset), length);
			if (curline == l.startLine && l.startPos > lineref.position())
				l.startPos = l.startPos < length ? 0 : l.startPos - length;
			if (curline == l.endLine && l.endPos > lineref.position())
				l.endPos = l.endPos < length ? 0 : l.endPos - length;
		}
		else // add keyword
		{
			editor()->insertAt(keywords[0], curline, lineref.position() + commentOffset);
			if (curline == l.startLine && l.startPos > lineref.position())
				l.startPos += 1;
			if (curline == l.endLine && l.endPos > lineref.position())
				l.endPos += 1;
		}
	}
	editor()->SendScintilla(QsciScintilla::SCI_ENDUNDOACTION);

	restoreSelection(editor(), l);
}
