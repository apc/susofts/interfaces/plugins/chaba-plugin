#include "ChabaPointLexerIO.hpp"

#include <algorithm>
#include <bitset>
#include <cctype>
#include <cmath>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include <Qsci/qsciabstractapis.h>
#include <Qsci/qsciscintilla.h>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <StringManager.h>
#include <editors/text/TextUtils.hpp>

#include "ChabaPlugin.hpp"
#include "ChabaProject.hpp"

namespace
{
inline int getPosition(std::istringstream &sstr)
{
	return sstr.eof() ? (int)sstr.str().size() : (int)sstr.tellg();
}

inline void resetStream(std::istringstream &sstr, const std::string &value = "")
{
	sstr.clear();
	sstr.str(value);
}

template<class T>
inline bool readValue(std::istringstream &sstr, T &value)
{
	sstr >> value;
	return !sstr.fail();
}

template<class T>
inline void addtostring(std::string &base, const T &value, size_t precision = 5)
{
	std::ostringstream sstr;
	if (!base.empty())
		sstr << '\t';
	sstr << std::fixed << std::setprecision(precision) << value;
	base += sstr.str();
}

class ChabaPointLexerIOAPIs : public QsciAbstractAPIs
{
public:
	ChabaPointLexerIOAPIs(ChabaPointLexerIO *lexer, ChabaPlugin *plugin = nullptr) :
		QsciAbstractAPIs(lexer), _lexer(lexer), _plugin(plugin), _referentials(QString(lexer->keywords(ChabaPointLexerIO::Styles::Referential)).split(' '))
	{
	}

	// QsciAbstractAPIs
	virtual void updateAutoCompletionList(const QStringList &context, QStringList &list) override
	{
		// test if we are at the beginning of the line
		if (context.size() > 1)
			return;

		QStringRef beginning;
		if (!context.isEmpty())
			beginning = &context.last();

		QStringList apis = _referentials;
		// get points
		{
			std::unordered_set<std::string> points = _plugin->_getProject(_lexer)->activePoints;
			const std::unordered_set<std::string> &pointstmp = _plugin->_getProject(_lexer)->passivePoints;
			points.insert(std::cbegin(pointstmp), std::cend(pointstmp));
			for (const auto &p : points)
				apis.append(fromByteArray(p.c_str(), _lexer->utf8()));
		}
		apis.sort(toQtCaseSensitivity());
		list.reserve(apis.size());
		for (const auto &api : apis)
		{
			if (api.compare(beginning, toQtCaseSensitivity()) < 0)
				continue;
			else if (api.startsWith(beginning, toQtCaseSensitivity()))
				list.append(api);
			else
				break;
		}
	}
	virtual QStringList callTips(const QStringList &, int, QsciScintilla::CallTipsStyle, QList<int> &) override { return QStringList(); }

private:
	Qt::CaseSensitivity toQtCaseSensitivity() { return _lexer->caseSensitive() ? Qt::CaseSensitivity::CaseSensitive : Qt::CaseSensitivity::CaseInsensitive; }

private:
	ChabaPointLexerIO *_lexer = nullptr;
	ChabaPlugin *_plugin = nullptr;
	const QStringList _referentials;
};
} // namespace

ChabaPointLexerIO::ChabaPointLexerIO(QObject *parent, ChabaPlugin *plugin) : ShareablePointsListIOLexer(parent), _plugin(plugin)
{
	// set APIs for autocompletion
	setAPIs(new ChabaPointLexerIOAPIs(this, _plugin));
}

const std::string &ChabaPointLexerIO::getMIMEType() const
{
	static const std::string mime = ChabaPlugin::_baseMimetype().toStdString() + ".pointRAW";
	return mime;
}

const char *ChabaPointLexerIO::keywords(int set) const
{
	set &= ~Styles::InactivePoint;
	set &= ~Styles::BadPoint;
	set &= ~Styles::PointRef;

	switch (set)
	{
	case Styles::Referential:
		return "*OLOC *RS2K *LEP *SPHE";
	case Styles::PointSigmas:
		return "SX SY SZ";
	}
	return nullptr;
}

QString ChabaPointLexerIO::description(int style) const
{
	style &= ~Styles::InactivePoint;
	style &= ~Styles::BadPoint;
	style &= ~Styles::PointRef;

	switch (style)
	{
	case Styles::Default:
		return "Default";
	case Styles::Comment:
		return "Comment";
	case Styles::PointName:
		return "Point Name";
	case Styles::PointCoordinates:
		return "Point Coordinates";
	case Styles::PointSigmas:
		return "Point Sigmas";
	case Styles::PointSigmaKeywords:
		return "Point Sgima Keyword";
	case Styles::Referential:
		return "Referential";
	case Styles::MaxStyle:
		return "MaxStyle";
	}

	return QString();
}

const char *ChabaPointLexerIO::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-*éèà";
}

bool ChabaPointLexerIO::defaultEolFill(int) const
{
	return false;
}

QColor ChabaPointLexerIO::defaultColor(int style) const
{
	style &= ~Styles::InactivePoint;
	style &= ~Styles::BadPoint;
	style &= ~Styles::PointRef;

	switch (style)
	{
	case Styles::Default:
		return Qt::GlobalColor::darkGray;
	case Styles::Comment:
		return Qt::GlobalColor::darkGreen;
	case Styles::PointName:
		return Qt::GlobalColor::darkCyan;
	case Styles::PointCoordinates:
		return Qt::GlobalColor::magenta;
	case Styles::PointSigmas:
	case Styles::PointSigmaKeywords:
		return Qt::GlobalColor::darkMagenta;
	case Styles::Referential:
		return Qt::GlobalColor::darkBlue;
	}

	return ShareablePointsListIOLexer::defaultColor(style);
}

QFont ChabaPointLexerIO::defaultFont(int style) const
{
	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	baseFont.setWeight(QFont::Weight::Light);

	if (style & Styles::InactivePoint || style & Styles::BadPoint)
	{
		baseFont.setItalic(true);
		style &= ~Styles::InactivePoint;
		style &= ~Styles::BadPoint;
	}
	if (style & Styles::PointRef)
	{
		baseFont.setWeight(QFont::Weight::Bold);
		style &= ~Styles::PointRef;
	}

	switch (style)
	{
	case Styles::Default:
		return baseFont;
	case Styles::Comment:
	case Styles::PointCoordinates:
	case Styles::PointSigmas:
		return baseFont;
	case Styles::PointName:
	case Styles::PointSigmaKeywords:
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	case Styles::Referential:
		baseFont.setItalic(true);
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	}
	return ShareablePointsListIOLexer::defaultFont(style);
}

QColor ChabaPointLexerIO::defaultPaper(int style) const
{
	return ShareablePointsListIOLexer::defaultPaper(style);
}

ShareablePointsList ChabaPointLexerIO::read(const std::string &contents)
{
	std::string copy = contents;
	ShareablePointsList spl;

	copy = trim(std::move(copy));
	spl.getParams() = readParams(copy);
	size_t idx = copy.find('\n');
	if (idx != std::string::npos && copy[0] == '*') // If multiline and first character is related to referential
		copy.erase(0, idx + 1);
	if (idx != std::string::npos || (idx == std::string::npos && copy[0] != '*')) // If multiline or just one line without referential (use the default one and treat line as a point)
	{
		// As setRootFrame override params, they have to be set here manually
		auto newFrame = std::make_unique<ShareableFrame>(readFrame(copy));
		newFrame->getParams() = spl.getParams();
		spl.setRootFrame(newFrame.release());
	}

	return spl;
}

ShareableExtraInfos ChabaPointLexerIO::readExtraInfos(const std::string &)
{
	return ShareableExtraInfos();
}

ShareableFrame ChabaPointLexerIO::readFrame(const std::string &contents)
{
	ShareableFrame frame(std::make_shared<ShareableParams>());

	std::istringstream stream(contents);
	std::string line;
	while (std::getline(stream, line))
	{
		if (isEmpty(line))
			continue;
		frame.add(new ShareablePoint(readPoint(line)));
	}
	return frame;
}

ShareableParams ChabaPointLexerIO::readParams(const std::string &contents)
{
	std::string tmp, cs;
	std::istringstream sstr(contents);

	if (contents.empty() || !readValue(sstr, cs) || cs.empty())
		throw SPIOException(std::string("Can't convert to a ShareableParams"), "", contents);
	if (contents[0] != '*' && !sstr.eof())
		cs = "*OLOC";
	else
	{
		int curpos = getPosition(sstr);
		std::getline(sstr, tmp);
		addStyle(curpos, Styles::Referential);
	}

	if (_hasReferential)
		throw SPIOException(std::string("There already is one referential in the editor"), "", "");

	auto keys = split(keywords(Styles::Referential));
	if ((_lexerMode ? false : !isEmpty(tmp)) || std::find(std::cbegin(keys), std::cend(keys), cs) == std::cend(keys))
		throw SPIOException(std::string("Can't convert to a ShareableParams"), "", contents);

	_isOLOC = (cs == "*OLOC");
	_referential = QString::fromStdString(cs);
	_hasReferential = true;
	return ShareableParams{(int)getPrecision(), _isOLOC ? ShareableParams::ECoordSys::k3DCartesian : ShareableParams::ECoordSys::k2DPlusH, {{"coordinateSystem", cs}}};
}

ShareablePoint ChabaPointLexerIO::readPoint(const std::string &contents)
{
	std::string tmp, name, dcum, id, option, comments;
	std::istringstream sstr(contents);
	int curpos = 0;
	bool active = true;
	QString qs;
	if (!_lexerMode)
	{
		_currentText = fromByteArray(contents.c_str(), utf8());
		initLexer();
		_styles.addLine(_currentText.size());
		_currentLine = &_currentText;
	}

	// name
	if (!readValue(sstr, name))
		throw SPIOException(std::string("Can't convert to a ShareablePoint"), "", contents);
	if (dealWithPointName(name).empty())
		name = "__dumy__";
	active = !_inactivePoint;
	curpos = getPosition(sstr);
	addIndicatorDestinationLink(_curpos + curpos - (int)name.size(), name, contents, tmp);
	addStyle(curpos, Styles::PointName);
	// position
	std::getline(sstr, tmp);
	_lastPoint = name;
	ShareablePosition pos = readPosition(tmp);
	// comment
	tmp = toByteArray(unstyledLine().toString(), utf8()).constData();
	curpos = (int)tmp.size();
	size_t idx = tmp.find('$');
	if (!tmp.empty() && idx != std::string::npos && isEmpty(tmp.substr(0, idx)))
	{
		tmp.erase(0, idx + 1);
		resetStream(sstr, tmp);
		sstr >> dcum;
		sstr >> id;
		sstr >> option;
		std::getline(sstr, comments);
		comments = trim(std::move(comments));
		addStyle(unstyledLine().size(), Styles::Comment);
	}
	else if (!isEmpty(tmp))
	{
		_badPoint = true;
		throw SPIOException("Can't convert to a ShareablePosition");
	}
	ShareableExtraInfos infos;
	if (!dcum.empty())
		infos.addExtraInfo("DCUM", dcum);
	if (!id.empty())
		infos.addExtraInfo("id", id);

	if (!_lexerMode)
		endLexer();

	return ShareablePoint{name, pos, comments, "", active, std::move(infos)};
}

ShareablePosition ChabaPointLexerIO::readPosition(const std::string &contents)
{
	std::string tmp;
	bool good;
	double x = 0, y = 0, z = std::numeric_limits<double>::quiet_NaN(), sigmax = 0, sigmay = 0, sigmaz = 0;
	std::istringstream sstr(contents);
	int curpos = 0;

	// x, y, z
	good = readValue(sstr, x);
	good = good && readValue(sstr, y);
	curpos = getPosition(sstr);
	std::getline(sstr, tmp);
	resetStream(sstr, tmp);
	if (readValue(sstr, z))
		curpos += getPosition(sstr);
	else
	{
		resetStream(sstr, tmp);
		z = std::numeric_limits<double>::quiet_NaN();
	}
	if (!good)
		throw SPIOException("Can't convert to a ShareablePosition", "", contents);
	addStyle(curpos, Styles::PointCoordinates);
	// sigmas
	if (!sstr.eof())
	{
		auto readSigma = [this, &good, &sstr, &curpos](const std::string &sigmakeyword, double &sigmavalue) -> void {
			std::string dummy;
			curpos = getPosition(sstr);

			good = good && readValue(sstr, dummy);
			int indicatorPos = _curpos;
			if (good && dummy == sigmakeyword)
			{
				addStyle(getPosition(sstr) - curpos, Styles::PointSigmaKeywords);
				if (_lexerMode)
				{
					indicatorPos = _curpos; // update after keyword read
					// Get sigma value exact position
					while (indicatorPos < _currentText.size() && _currentText[indicatorPos].isSpace())
						indicatorPos++;
					// Mark it
					addIndicatorDestinationLink(indicatorPos, _lastPoint + "//" + sigmakeyword, "", "");
				}
				// Read the value and style it
				curpos = getPosition(sstr);
				good = good && readValue(sstr, sigmavalue);
				if (good)
				{
					addStyle(getPosition(sstr) - curpos, Styles::PointSigmas);
					if (sigmavalue == 0 && std::signbit(sigmavalue))
						sigmavalue = -0.001;
				}
				else
				{
					_badPoint = true;
					throw SPIOException("Can't convert to a ShareablePosition");
				}
			}
		};
		readSigma("SX", sigmax);
		if (std::isnan(sigmax))
			sigmax = 0;
		readSigma("SY", sigmay);
		if (std::isnan(sigmay))
			sigmay = 0;
		readSigma("SZ", sigmaz);
		if (std::isnan(sigmaz))
			sigmaz = 0;
	}
	return ShareablePosition{x, y, z, sigmax, sigmay, sigmaz};
}

std::string ChabaPointLexerIO::write(const ShareablePointsList &list)
{
	std::string result;
	if (exportFieldsPointsList.hasField("params"))
		result = write(list.getParams());
	if (exportFieldsPointsList.hasField("rootFrame"))
		result += write(list.getRootFrame());
	return result;
}

std::string ChabaPointLexerIO::write(const ShareableFrame &frame)
{
	if (!exportFieldsFrame.hasField("points"))
		return std::string();

	// template lambda bitch!
	auto writepoints = [this](const auto &points) -> std::string {
		std::string result;
		for (const auto &p : points)
			result += write(*p) + '\n';
		return result;
	};

	if (exportFieldsFrame.hasField("innerFrames"))
		return writepoints(frame.getAllPoints()); // vector of naked pointers
	return writepoints(frame.getPoints()); // vector of unique_ptr (to go faster)
}

std::string ChabaPointLexerIO::write(const ShareableParams &params)
{
	std::string result;
	if (params.extraInfos.has("coordinateSystem"))
		addtostring(result, params.extraInfos.at("coordinateSystem") + '\n');
	return result;
}

std::string ChabaPointLexerIO::write(const ShareablePoint &point)
{
	std::string result;
	addtostring(result, point.active ? point.name : '!' + point.name);
	addtostring(result, write(point.position));
	if (exportFieldsPoint.hasField("extraInfos")
		&& (point.extraInfos.has("DCUM") || point.extraInfos.has("id") || (exportFieldsPoint.hasField("inlineComment") && !point.inlineComment.empty())))
	{
		// DCUM and ID
		addtostring(result, '$' + (point.extraInfos.has("DCUM") ? point.extraInfos.at("DCUM") : "0"));
		addtostring(result, (point.extraInfos.has("id") ? point.extraInfos.at("id") : "0"));
		// option
		if (!point.position.isfreex && !point.position.isfreey && !point.position.isfreez)
			addtostring(result, "CALA");
		else if (point.position.isfreex && point.position.isfreey && point.position.isfreez)
			addtostring(result, "POIN");
		else if (point.position.isfreex && point.position.isfreey && !point.position.isfreez)
			addtostring(result, "VXY");
		else if (point.position.isfreex && !point.position.isfreey && point.position.isfreez)
			addtostring(result, "VXZ");
		else if (!point.position.isfreex && point.position.isfreey && point.position.isfreez)
			addtostring(result, "VYZ");
		else if (!point.position.isfreex && !point.position.isfreey && point.position.isfreez)
			addtostring(result, "VZ");
		// comment
		addtostring(result, point.inlineComment);
	}
	return result;
}

std::string ChabaPointLexerIO::write(const ShareablePosition &position)
{
	std::string result;
	// x, y, z
	addtostring(result, position.x, getPrecision());
	addtostring(result, position.y, getPrecision());
	if (!std::isnan(position.z))
		addtostring(result, position.z, getPrecision());
	// sigma
	addtostring(result, "SX");
	addtostring(result, position.sigmax, getPrecision());
	addtostring(result, "SY");
	addtostring(result, position.sigmay, getPrecision());
	if (!std::isnan(position.z))
	{
		addtostring(result, "SZ");
		addtostring(result, position.sigmaz, getPrecision());
	}
	return result;
}

void ChabaPointLexerIO::styleText()
{
	_inactivePoint = false;
	_badPoint = false;
	_refPoint = false;
	QStringRef line = _currentLine.trimmed();

	if (line.isEmpty())
	{
		_infoAnnotation = ChabaPlugin::_HELP_STRING.arg(_activeMode ? tr("Active (reference)") : tr("Passive"));
		return;
	}
	else if (line[0] == '*') // coordinate system
	{
		_infoAnnotation = ChabaPlugin::_REFERENTIAL_STRING;
		try
		{
			readParams(toByteArray(_currentLine.toString(), utf8()).constData());
		}
		catch (...)
		{
			_badPoint = true;
		}
	}
	else if (line[0] == '%') // comment
	{
		addStyle(unstyledLine().size(), Styles::Comment);
	}
	else // point
	{
		try
		{
			readPoint(toByteArray(_currentLine.toString(), utf8()).constData());
			_infoAnnotation = ChabaPlugin::_POINT_STRING;
		}
		catch (...)
		{
			_infoAnnotation = ChabaPlugin::_HELP_STRING.arg(_activeMode ? tr("Active (reference)") : tr("Passive"));
			_badPoint = true;
		}
	}

	if (_badPoint)
	{
		_errorAnnotation += ChabaPlugin::_POINTINVALID_STRING;
		addMarker(editor(), _curpos - 1, TextMarker::ERROR);
	}
	else
	{
		if (_inactivePoint)
			_errorAnnotation += ChabaPlugin::_POINTDEAC_STRING;
		if (_refPoint)
		{
			_errorAnnotation += ChabaPlugin::_POINTREF_STRING;
			addMarker(editor(), _curpos - 1, Markers::Reference);
		}
	}
	_inactivePoint = false;
	_badPoint = false;
	_refPoint = false;
}

bool ChabaPointLexerIO::startLexer(int, int)
{
	if (!editor())
		return false;
	if (_plugin && _plugin->_getProject(this))
	{
		if (_activeMode)
			_plugin->_getProject(this)->activePoints.clear();
		else
			_plugin->_getProject(this)->passivePoints.clear();
		_hasReferential = false;
	}
	editor()->markerDeleteAll(Markers::Reference);
	_measures.clearAll();
	// trick to always analyse the whole text
	// if we see that analyse become too slow, we can try to improve the lexer here
	// this is needed to keep the sets of active and passive points up to date
	return ShareablePointsListIOLexer::startLexer(0, editor()->length());
}

void ChabaPointLexerIO::addStyle(int offset, int style)
{
	if (_inactivePoint || _badPoint)
		style |= Styles::InactivePoint;
	if (_refPoint)
		style |= Styles::PointRef;
	ShareablePointsListIOLexer::addStyle(offset, style);
}

std::string &ChabaPointLexerIO::dealWithPointName(std::string &pointName)
{
	_inactivePoint = false;
	if (pointName.empty() || pointName.find_first_of(_forbiddenPointCharacters) != std::string::npos)
	{
		_badPoint = true;
		return pointName;
	}
	if (pointName[0] == '!' || pointName[0] == '?')
	{
		_inactivePoint = true;
		pointName.erase(0, 1);
	}
	if (!_lexerMode)
		return pointName;

	if (!_inactivePoint && _plugin)
	{
		auto &project = *_plugin->_getProject(this);
		auto &toinsert = _activeMode ? project.activePoints : project.passivePoints;
		const auto &tocheck = _activeMode ? project.passivePoints : project.activePoints;

		const auto &[it, dontexist] = toinsert.insert(pointName);
		if (!dontexist)
			_badPoint = true;
		else
			_refPoint = tocheck.find(pointName) != std::cend(tocheck);
	}

	return pointName;
}

void ChabaPointLexerIO::addIndicatorDestinationLink(int position, const std::string &name, const std::string &representation, const std::string &header)
{
	_measures.points[hashString(name)] = ChabaMeasures::Value{
		position, QString::fromStdString(name), QString::fromStdString(representation), _referential, "", QString::fromStdString(header)};
}
