/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPOINTLEXERIO_HPP
#define CHABAPOINTLEXERIO_HPP

#include <editors/text/ShareablePointsListIOLexer.hpp>
#include <editors/text/TextUtils.hpp>

#include "ChabaMeasures.hpp"

class ChabaPlugin;

/**
 * Lexer for input points in Chaba.
 */
class ChabaPointLexerIO : public ShareablePointsListIOLexer
{
	Q_OBJECT

public:
	ChabaPointLexerIO(QObject *parent = nullptr, ChabaPlugin *plugin = nullptr);
	virtual ~ChabaPointLexerIO() override = default;

	enum Markers
	{
		/** reference marker */
		Reference = TextMarker::INFO + 2
	};
	// QsciLexerCustom
	enum Styles
	{
		/** Default */
		Default,
		/** Comment / ignored part */
		Comment,
		/** Name of a point */
		PointName,
		/** Coordinates of a point */
		PointCoordinates,
		/** Sigmas of a point */
		PointSigmas,
		/** SX, SY and SZ */
		PointSigmaKeywords,
		/** Coordinate system */
		Referential,
		/** End of enum */
		MaxStyle,
		/** Flag when the point is a reference (is present in both actif and passive) */
		PointRef = 0b10000, // 16
		/** Flag for inactive points (prepended with '!' or '?') */
		InactivePoint = 0b1000000, // 64
		/** Flag for bad points */
		BadPoint = 0b10000000 // 128
	};
	virtual const char *language() const override { return "Chaba Active or Passive"; }
	virtual const char *keywords(int set) const override;
	virtual QString description(int style) const override;
	virtual const char *wordCharacters() const override;
	virtual bool defaultEolFill(int style) const override;
	QColor defaultColor(int style) const override;
	QFont defaultFont(int style) const override;
	QColor defaultPaper(int style) const override;

	// IShareablePointsListIO
	virtual const std::string &getMIMEType() const override;
	virtual ShareablePointsList read(const std::string &contents) override;
	virtual ShareableExtraInfos readExtraInfos(const std::string &contents) override;
	virtual ShareableFrame readFrame(const std::string &contents) override;
	virtual ShareableParams readParams(const std::string &contents) override;
	virtual ShareablePoint readPoint(const std::string &contents) override;
	virtual ShareablePosition readPosition(const std::string &contents) override;
	virtual std::string write(const ShareablePointsList &list) override;
	virtual std::string write(const ShareableExtraInfos &) override { return std::string(); }
	virtual std::string write(const ShareableFrame &frame) override;
	virtual std::string write(const ShareableParams &params) override;
	virtual std::string write(const ShareablePoint &point) override;
	virtual std::string write(const ShareablePosition &position) override;

	// ChabaPointLexerIO
	/** says if the class is in active or passive mode */
	bool activeMode() const noexcept { return _activeMode; }
	/** put the class in active (reference) mode, or in passive mode  (default is active) */
	void activeMode(bool active) noexcept { _activeMode = active; }
	const ChabaMeasures &chabaMeasures() const noexcept { return _measures; }

	/** @return the precision set in the plugin */
	size_t getPrecision() { return _precision; }
	/** Set the precision for the plugin */
	void setPrecision(size_t precision) { _precision = precision; }

protected:
	// ShareablePointsListIOLexer
	virtual void styleText() override;
	virtual bool startLexer(int, int) override;

private:
	/**
	 * Add a style to the _currentLine.
	 * @param offset the number of character to style
	 * @param style the style to apply
	 */
	void addStyle(int offset, int style);
	/** deal with the active and passive sets of points */
	std::string &dealWithPointName(std::string &pointName);
	/** Add an entry in measures for external editors to link to */
	void addIndicatorDestinationLink(int position, const std::string &name, const std::string &representation, const std::string &header);

private:
	/** plugin */
	ChabaPlugin *_plugin = nullptr;
	/** Tells if we are in active (reference) or passive mode */
	bool _activeMode = true;
	/** Tells if we are in OLOC (cartesian) or not (2D+H) */
	bool _isOLOC = true;
	/** Tells if there is already a referential. */
	int _hasReferential = false;
	/** true if the point is inactive */
	bool _inactivePoint = false;
	/** The last analysed point name */
	std::string _lastPoint;
	/** true if point is bad */
	bool _badPoint = false;
	/** true if the point is part of the reference points */
	bool _refPoint = false;
	/** Precision of the lexer */
	size_t _precision = 5;
	/** Characters not permitted in a point name */
	const std::string _forbiddenPointCharacters = "@#$%^&*,;";
	/** Referential sysyem */
	QString _referential;
	/** Measures for indicator link with CTRL + MOUSE CLICK */
	ChabaMeasures _measures;
};

#endif // CHABAPOINTLEXERIO_HPP
