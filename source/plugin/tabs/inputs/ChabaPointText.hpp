/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPOINTTEXT_HPP
#define CHABAPOINTTEXT_HPP

#include <editors/text/TextEditorWidget.hpp>

struct ChabaMeasures;

/**
 * Text editor for input points in Chaba.
 *
 * This class uses ChabaPointLexerIO.
 */
class ChabaPointText : public TextEditorWidget
{
	Q_OBJECT

public:
	ChabaPointText(SPluginInterface *owner, bool activeMode, QWidget *parent = nullptr);
	virtual ~ChabaPointText() override = default;

	// SGraphicalWidget
	virtual ShareablePointsList getContent() const override;

	// ChabaPointText
	const ChabaMeasures &chabaMeasures();

public slots:
	// SGraphicalWidget
	virtual void setContent(const ShareablePointsList &spl) override;
	virtual void updateContent() override;
	virtual void updateUi(const QString &pluginName) override;
	void globalIndicatorClicked(const QString &text, int identifier);

protected:
	// SGraphicalWidget
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

private:
	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);
	size_t getPrecision() const;
	/** Disables or enables a point by prepending the point name with one exclamation mark. */
	void toggleKeyword(const QString &keyword);

private:
	/** Tells if we are in active (reference) or passive mode */
	const bool _activeMode = true;
	bool _cooMode = false;
};

#endif // CHABAPOINTTEXT_HPP
