#include "ChabaPointEditor.hpp"

#include <memory>

#include <interface/SGraphicalInterface.hpp>
#include <utils/SettingsManager.hpp>

#include "ChabaPlugin.hpp"
#include "tabs/inputs/ChabaPointGui.hpp"
#include "tabs/inputs/ChabaPointTableEditor.hpp"
#include "tabs/inputs/ChabaPointText.hpp"
#include "trinity/ChabaConfig.hpp"

class ChabaPointEditor::_ChabaPointEditor_pimpl
{
public:
	/** text */
	ChabaPointText *textEditor = nullptr;
	/** gui */
	ChabaPointGui *guiEditor = nullptr;
};

ChabaPointEditor::ChabaPointEditor(SPluginInterface *owner, bool activeMode, QWidget *parent) :
	STabInterface(owner, parent), _pimpl(std::make_unique<_ChabaPointEditor_pimpl>())
{
	enablefileWatcher(true);

	auto config = SettingsManager::settings().settings<ChabaConfigObject>(ChabaPlugin::_name());
	if (config.showTextEditors)
	{
		_pimpl->textEditor = new ChabaPointText(owner, activeMode, this);
		_pimpl->textEditor->setWindowTitle("Text editor");
		installEventFilterAll(this, _pimpl->textEditor);
		tab()->addTab(_pimpl->textEditor, _pimpl->textEditor->windowTitle());
	}
	if (config.showGUIEditors)
	{
		_pimpl->guiEditor = new ChabaPointGui(new ChabaPointTableEditor(dynamic_cast<ChabaPlugin *>(owner), activeMode, this), owner, activeMode, this);
		_pimpl->guiEditor->setWindowTitle("Table editor");
		installEventFilterAll(this, _pimpl->guiEditor);
		tab()->addTab(_pimpl->guiEditor, _pimpl->guiEditor->windowTitle());
	}
}

ChabaPointEditor::~ChabaPointEditor() = default;

const ChabaMeasures &ChabaPointEditor::chabaMeasures()
{
	return _pimpl->textEditor->chabaMeasures();
}

void ChabaPointEditor::globalIndicatorClicked(const QString &text, int position)
{
	tab()->setCurrentIndex(0);
	_pimpl->textEditor->globalIndicatorClicked(text, position);
}
