#include "ChabaOutputLexer.hpp"

#include <algorithm>
#include <bitset>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include <Qsci/qsciscintilla.h>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <StringManager.h>
#include <editors/text/TextUtils.hpp>

#include "ChabaPlugin.hpp"
#include "ChabaProject.hpp"

namespace
{
const QString HELP_STRING = QObject::tr(R"""(
	<h3>Output file</h3>
	<p>The result file is the main output file summarizing the results. It always written.</p>
	<p>The structure of the file is:</p>
	<ul>
		<li>Header</li>
		<li>Information about calculation and project</li>
		<li>Estimated Helmert transformations</li>
		<li>Different lists of points</li>
	</ul>
)""");

inline int getPosition(std::istringstream &sstr)
{
	return sstr.eof() ? (int)sstr.str().size() : (int)sstr.tellg();
}

inline void resetStream(std::istringstream &sstr, const std::string &value = "")
{
	sstr.clear();
	sstr.str(value);
}

template<class T>
inline bool readValue(std::istringstream &sstr, T &value)
{
	sstr >> value;
	return !sstr.fail();
}
} // namespace

const std::string &ChabaOutputLexer::getMIMEType() const
{
	static const std::string mime = ChabaPlugin::_baseMimetype().toStdString() + ".outputpointRAW";
	return mime;
}

const char *ChabaOutputLexer::keywords(int set) const
{
	switch (set)
	{
	case Styles::Comment:
		return "coordinate system reference frame";
	}
	return nullptr;
}

QString ChabaOutputLexer::description(int style) const
{
	style &= ~Styles::InactivePoint;
	style &= ~Styles::PointRef;

	switch (style)
	{
	case Styles::Default:
		return "Default";
	case Styles::Comment:
		return "Comment";
	case Styles::PointName:
		return "Point Name";
	case Styles::PointCoordinates:
		return "Point Coordinates";
	case Styles::PointOther:
		return "Point Other";
	case Styles::MaxStyle:
		return "MaxStyle";
	case Styles::CalculatedParameters:
		return "Calculated Parameters";
	}

	return QString();
}

const char *ChabaOutputLexer::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";
}

bool ChabaOutputLexer::defaultEolFill(int) const
{
	return false;
}

QColor ChabaOutputLexer::defaultColor(int style) const
{
	style &= ~Styles::InactivePoint;
	style &= ~Styles::PointRef;

	switch (style)
	{
	case Styles::Default:
		return Qt::GlobalColor::darkGray;
	case Styles::Comment:
		return Qt::GlobalColor::darkGreen;
	case Styles::PointName:
		return Qt::GlobalColor::darkCyan;
	case Styles::PointCoordinates:
		return Qt::GlobalColor::magenta;
	case Styles::PointOther:
		return Qt::GlobalColor::darkCyan;
	case Styles::CalculatedParameters:
		return Qt::GlobalColor::blue;
	}

	return ShareablePointsListIOLexer::defaultColor(style);
}

QFont ChabaOutputLexer::defaultFont(int style) const
{
	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	baseFont.setWeight(QFont::Weight::Light);

	if (style & Styles::InactivePoint)
	{
		baseFont.setItalic(true);
		style &= ~Styles::InactivePoint;
	}
	if (style & Styles::PointRef)
	{
		baseFont.setWeight(QFont::Weight::Bold);
		style &= ~Styles::PointRef;
	}

	switch (style)
	{
	case Styles::Default:
		return baseFont;
	case Styles::Comment:
	case Styles::PointCoordinates:
	case Styles::PointOther:
		return baseFont;
	case Styles::CalculatedParameters:
	case Styles::PointName:
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	}
	return ShareablePointsListIOLexer::defaultFont(style);
}

QColor ChabaOutputLexer::defaultPaper(int style) const
{
	return ShareablePointsListIOLexer::defaultPaper(style);
}

ShareablePointsList ChabaOutputLexer::read(const std::string &contents)
{
	ShareablePointsList spl;

	spl.setRootFrame(new ShareableFrame(readFrame(contents)));

	return spl;
}

ShareableExtraInfos ChabaOutputLexer::readExtraInfos(const std::string &)
{
	return ShareableExtraInfos();
}

ShareableFrame ChabaOutputLexer::readFrame(const std::string &contents)
{
	ShareableFrame frame(std::make_shared<ShareableParams>());

	std::istringstream stream(contents);
	std::string line;
	while (std::getline(stream, line))
	{
		if (isEmpty(line))
			continue;
		frame.add(new ShareablePoint(readPoint(line)));
	}
	return frame;
}

ShareableParams ChabaOutputLexer::readParams(const std::string &)
{
	return ShareableParams();
}

ShareablePoint ChabaOutputLexer::readPoint(const std::string &contents)
{
	std::string tmp, name, comments;
	std::istringstream sstr(contents);
	int curpos = 0;
	bool active = true;
	QString qs;
	if (!_lexerMode)
	{
		_styles.addLine((int)contents.size());
		qs = fromByteArray(contents.c_str(), utf8());
		_currentLine = &qs;
	}

	// name
	if (!readValue(sstr, name))
		throw SPIOException(std::string("Can't convert to a ShareablePoint"), "", contents);
	dealWithPointName(name);
	// point indicator
	if (contains(_inMeasures.points, name))
		addIndication(editor(), TextIndicator::EXTERNAL, _curpos, (int)name.size(), hashString(name));
	// style
	active = !_inactivePoint;
	curpos = getPosition(sstr);
	addStyle(curpos, Styles::PointName);
	// position
	std::getline(sstr, tmp);
	ShareablePosition pos = readPosition(tmp);
	// comment
	tmp = toByteArray(unstyledLine().toString(), utf8()).constData();
	ShareableExtraInfos infos;
	comments = trim(tmp);
	addStyle((int)tmp.size(), Styles::PointOther);

	if (!_lexerMode)
	{
		_styles.clear();
		_currentLine.clear();
	}

	return ShareablePoint{name, pos, comments, "", active, std::move(infos)};
}

ShareablePosition ChabaOutputLexer::readPosition(const std::string &contents)
{
	bool good;
	std::istringstream sstr(contents);
	int lineStartPos = _curpos - _styles.getPosline() - 1;

	// x, y, z
	double x = 0, y = 0, z = 0;
	good = readValue(sstr, x);
	good = good && readValue(sstr, y);
	good = good && readValue(sstr, z);

	if (good)
		addStyle(getPosition(sstr), Styles::PointCoordinates);
	else
		throw SPIOException("Can't convert to a ShareablePosition", "", contents);

	if (!contains(_inMeasures.points, _lastPoint))
		return ShareablePosition{x, y, z};

	// sx, sy, zz
	double sx = 0, sy = 0, sz = 0;
	int pos1, pos2, pos3;
	good = good && readValue(sstr, sx);
	pos1 = getPosition(sstr);
	good = good && readValue(sstr, sy);
	pos2 = getPosition(sstr);
	good = good && readValue(sstr, sz);
	pos3 = getPosition(sstr);
	if (!good)
		return ShareablePosition{x, y, z};

	addIndication(editor(), TextIndicator::EXTERNAL, lineStartPos + pos1, pos2 - pos1, hashString(_lastPoint + "//SX"));
	addIndication(editor(), TextIndicator::EXTERNAL, lineStartPos + pos2, pos3 - pos2, hashString(_lastPoint + "//SY"));
	addIndication(editor(), TextIndicator::EXTERNAL, lineStartPos + pos3, pos3 - pos2, hashString(_lastPoint + "//SZ"));

	return ShareablePosition{x, y, z, sx, sy, sz};
}

void ChabaOutputLexer::styleText()
{
	_inactivePoint = false;
	_refPoint = false;
	QStringRef line = _currentLine.trimmed();
	_infoAnnotation = HELP_STRING;
	if (_currentLine.isEmpty()) // auto default
		return;

	// If calculated parameters
	if (!line.isEmpty() && line[0] == '*' && line.endsWith('*') && std::count(line.begin(), line.end(), '*') == 2 && line.lastIndexOf('=') != -1)
	{
		readParameter(toByteArray(_currentLine.toString(), utf8()).constData());
		return;
	}

	// If comment
	if (!line.isEmpty() && (_currentLine[0] == ' ' || line[0] == '*'))
	{
		readComment(toByteArray(_currentLine.toString(), utf8()).constData());
		return;
	}

	try
	{
		readPoint(toByteArray(_currentLine.toString(), utf8()).constData());
	}
	catch (...)
	{
	}
	if (_refPoint)
		addMarker(editor(), _curpos - 1, Markers::Reference);
	_inactivePoint = false;
	_refPoint = false;
}

bool ChabaOutputLexer::startLexer(int, int)
{
	// trick to always analyse the whole text
	// if we see that analyse become too slow, we can try to improve the lexer here
	// this is needed to keep the sets of active and passive points up to date
	if (!editor())
		return false;
	editor()->markerDeleteAll(Markers::Reference);
	return ShareablePointsListIOLexer::startLexer(0, editor()->length());
}

QString ChabaOutputLexer::calltip(int, int, const QString &indicator) const
{
	ChabaMeasures::Value value;

	if (contains(_inMeasures.points, indicator))
		value = _inMeasures.points.at(hashString(indicator));
	else
		return QString();

	return tr("  %1 %2\n  %3\n  %4").arg(value.type, value.name, value.representation, value.others);
}

void ChabaOutputLexer::addStyle(int offset, int style)
{
	if (_inactivePoint)
		style |= Styles::InactivePoint;
	if (_refPoint)
		style |= Styles::PointRef;
	ShareablePointsListIOLexer::addStyle(offset, style);
}

std::string &ChabaOutputLexer::dealWithPointName(std::string &pointName)
{
	_inactivePoint = false;
	if (pointName.empty())
		return pointName;
	if (pointName[0] == '!' || pointName[0] == '?')
	{
		_inactivePoint = true;
		pointName.erase(0, 1);
	}
	if (!_lexerMode)
		return pointName;

	if (_plugin)
	{
		auto *project = _plugin->_getProject(this);
		if (project)
			_refPoint = project->activePoints.find(pointName) != std::end(project->activePoints) && project->passivePoints.find(pointName) != std::end(project->passivePoints);
	}
	_lastPoint = pointName;

	return pointName;
}

void ChabaOutputLexer::readComment(std::string line)
{
	addStyle((int)line.size(), Styles::Comment);
}

void ChabaOutputLexer::readParameter(std::string line)
{
	size_t firstAsterisk = line.find('*') + 1;
	size_t lastAsterisk = line.find_last_of('*');
	addStyle(int(firstAsterisk), Styles::Comment);
	addStyle(int(lastAsterisk - firstAsterisk), Styles::CalculatedParameters);
	addStyle(int(line.size() - lastAsterisk), Styles::Comment);
}
