#include "ChabaOutputText.hpp"

#include <fstream>
#include <sstream>

#include <QMimeData>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/MarkerViewer.hpp>
#include <editors/text/TextEditor.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "ChabaMeasures.hpp"
#include "ChabaPlugin.hpp"
#include "tabs/outputs/ChabaOutputLexer.hpp"
#include "trinity/ChabaConfig.hpp"

ChabaOutputText::ChabaOutputText(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent)
{
	enablefileWatcher(true);
	installEventFilterAll(this, this);
	updateUi(owner->name());

	// setup editor
	editor()->setMimeTypesSupport(
		{clipboardMimeType().toStdString()}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
	// reference marker
	editor()->markerDefine(QPixmap(":/markers/reference").scaled(14, 14), ChabaOutputLexer::Markers::Reference);
	std::bitset<32> marginmask = editor()->marginMarkerMask(TextMargin::MARKERREPORT);
	marginmask[ChabaOutputLexer::Markers::Reference] = true;
	editor()->setMarginMarkerMask(TextMargin::MARKERREPORT, (int)marginmask.to_ulong());
	auto markers = markerDefinitions;
	markers[ChabaOutputLexer::Markers::Reference] = {tr("Reference"), ":/markers/reference"};
	markerViewer()->updateFilters(std::move(markers));
}

void ChabaOutputText::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);
	if (pluginName != ChabaPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<ChabaConfigObject>(pluginName);

	if (!config.color)
		editor()->setLexer(nullptr);
	else if (!editor()->lexer())
		editor()->setLexer(new ChabaOutputLexer(this, dynamic_cast<ChabaPlugin *>(owner())));
	setBackground(editor(), getThemeColor(QColor(240, 240, 240)));
}

void ChabaOutputText::chabaMeasures(const ChabaMeasures &measures)
{
	auto lex = qobject_cast<ChabaOutputLexer *>(editor()->lexer());
	if (lex)
		lex->chabaMeasures(measures);
}

const ChabaMeasures &ChabaOutputText::chabaMeasures()
{
	static const ChabaMeasures _m;
	auto lex = qobject_cast<ChabaOutputLexer *>(editor()->lexer());
	return lex ? lex->chabaMeasures() : _m;
}

QString ChabaOutputText::clipboardMimeType() const
{
	return ChabaPlugin::_baseMimetype() + ".outputpointRAW";
}

QByteArray ChabaOutputText::fromMime(const QMimeData *mimedata)
{
	ChabaOutputLexer io;
	io.utf8(editor()->isUtf8());
	return contentFromMime(mimedata, io);
}

QMimeData *ChabaOutputText::toMime(const QByteArray &text, QMimeData *mimedata)
{
	ChabaOutputLexer io;
	io.utf8(editor()->isUtf8());
	return contentToMime(text, mimedata, io);
}
