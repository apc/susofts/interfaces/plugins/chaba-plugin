#include "ChabaCooText.hpp"

#include <QMimeData>

#include <editors/text/MarkerViewer.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "ChabaPlugin.hpp"
#include "tabs/ChabaCooLexer.hpp"
#include "trinity/ChabaConfig.hpp"

ChabaCooText::ChabaCooText(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent)
{
	enablefileWatcher(true);
	installEventFilterAll(this, this);
	updateUi(owner->name());

	// setup editor
	editor()->setMimeTypesSupport(
		{clipboardMimeType().toStdString()}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
	// reference marker
	editor()->markerDefine(QPixmap(":/markers/reference").scaled(14, 14), ChabaCooLexer::Marker::Reference);
	std::bitset<32> marginmask = editor()->marginMarkerMask(TextMargin::MARKERREPORT);
	marginmask[ChabaCooLexer::Marker::Reference] = true;
	editor()->setMarginMarkerMask(TextMargin::MARKERREPORT, (int)marginmask.to_ulong());
	auto markers = markerDefinitions;
	markers[ChabaCooLexer::Marker::Reference] = {tr("Reference"), ":/markers/reference"};
	markerViewer()->updateFilters(std::move(markers));
}

void ChabaCooText::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);

	if (pluginName != ChabaPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<ChabaConfigObject>(pluginName);

	if (!config.color)
		editor()->setLexer(nullptr);
	else
	{
		if (config.color && !editor()->lexer())
		{
			auto *lexer = new ChabaCooLexer(this, dynamic_cast<ChabaPlugin *>(owner()));
			lexer->type(ChabaCooLexer::Type::Out);
			editor()->setLexer(lexer);
		}
		updateContent();
	}
	setBackground(editor(), getThemeColor(QColor(240, 240, 240)));
}

QString ChabaCooText::clipboardMimeType() const
{
	return QString::fromStdString(ChabaCooLexer().getMIMEType());
}

QByteArray ChabaCooText::fromMime(const QMimeData *mimedata)
{
	ChabaCooLexer io;
	io.utf8(editor()->isUtf8());
	return contentFromMime(mimedata, io);
}

QMimeData *ChabaCooText::toMime(const QByteArray &text, QMimeData *mimedata)
{
	ChabaCooLexer io;
	io.utf8(editor()->isUtf8());
	auto lex = qobject_cast<ChabaCooLexer *>(editor()->lexer());
	if (lex)
		io.columnsHeader(lex->columnsHeader());
	return contentToMime(text, mimedata, io);
}
