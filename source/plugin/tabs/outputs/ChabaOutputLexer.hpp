/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAOUTPUTLEXER_HPP
#define CHABAOUTPUTLEXER_HPP

#include <editors/text/ShareablePointsListIOLexer.hpp>
#include <editors/text/TextUtils.hpp>

#include "ChabaMeasures.hpp"

class ChabaPlugin;

/**
 * Lexer for Chaba ouptput points.
 */
class ChabaOutputLexer : public ShareablePointsListIOLexer
{
	Q_OBJECT

public:
	ChabaOutputLexer(QObject *parent = nullptr, ChabaPlugin *plugin = nullptr) : ShareablePointsListIOLexer(parent), _plugin(plugin) {}
	virtual ~ChabaOutputLexer() override = default;

	enum Markers
	{
		/** reference marker */
		Reference = TextMarker::INFO + 2
	};
	// QsciLexerCustom
	enum Styles
	{
		/** Default */
		Default,
		/** Comment / ignored part */
		Comment,
		/** Name of a point */
		PointName,
		/** Coordinates of a point */
		PointCoordinates,
		/** Other like DCUM... */
		PointOther,
		/** End of enum */
		MaxStyle,
		/** Calculated results parameters */
		CalculatedParameters,
		/** Flag when the point is a reference (is present in both actif and passive) */
		PointRef = 0b10000, // 16
		/** Flag for inactive points (prepended with '!' or '?') */
		InactivePoint = 0b1000000, // 64
	};

	/**
	 * Uses the input measures to create links between input and output file.
	 *
	 * @warning This method must be called before opening the output file.
	 */
	void chabaMeasures(ChabaMeasures measures) noexcept { _inMeasures = std::move(measures); }
	const ChabaMeasures &chabaMeasures() const noexcept { return _inMeasures; }

	virtual const char *language() const override { return "Chaba Out file"; }
	virtual const char *keywords(int set) const override;
	virtual QString description(int style) const override;
	virtual const char *wordCharacters() const override;
	virtual bool defaultEolFill(int style) const override;
	QColor defaultColor(int style) const override;
	QFont defaultFont(int style) const override;
	QColor defaultPaper(int style) const override;

	// IShareablePointsListIO
	virtual const std::string &getMIMEType() const override;
	virtual ShareablePointsList read(const std::string &contents) override;
	virtual ShareableExtraInfos readExtraInfos(const std::string &) override;
	virtual ShareableFrame readFrame(const std::string &contents) override;
	virtual ShareableParams readParams(const std::string &) override;
	virtual ShareablePoint readPoint(const std::string &contents) override;
	virtual ShareablePosition readPosition(const std::string &contents) override;
	virtual std::string write(const ShareablePointsList &) override { return std::string(); }
	virtual std::string write(const ShareableExtraInfos &) override { return std::string(); }
	virtual std::string write(const ShareableFrame &) override { return std::string(); }
	virtual std::string write(const ShareableParams &) override { return std::string(); }
	virtual std::string write(const ShareablePoint &) override { return std::string(); }
	virtual std::string write(const ShareablePosition &) override { return std::string(); }

protected:
	// ShareablePointsListIOLexer
	virtual void styleText() override;
	virtual bool startLexer(int, int) override;
	// TextLexer
	virtual QString calltip(int position, int indicatorType, const QString &indicator) const override;

private:
	/**
	 * Add a style to the _currentLine.
	 * @param offset the number of character to style
	 * @param style the style to apply
	 */
	void addStyle(int offset, int style);
	/** deal with the active and passive sets of points */
	std::string &dealWithPointName(std::string &pointName);
	/** Read a comment */
	void readComment(std::string line);
	/** Read parameters */
	void readParameter(std::string line);

private:
	/** plugin */
	ChabaPlugin *_plugin = nullptr;
	/** true if the point is inactive */
	bool _inactivePoint = false;
	/** true if the point is part of the reference points */
	bool _refPoint = false;
	/** last analysed point name */
	std::string _lastPoint;
	/** input measures */
	ChabaMeasures _inMeasures;
};

#endif // CHABAOUTPUTLEXER_HPP
