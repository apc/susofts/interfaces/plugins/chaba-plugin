/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABACOOTEXT_HPP
#define CHABACOOTEXT_HPP

#include <editors/text/TextEditorWidget.hpp>

class QMimeData;

/**
 * Text editor for output points in Chaba.
 *
 * This class uses ChabaCooLexer.
 */
class ChabaCooText : public TextEditorWidget
{
	Q_OBJECT

public:
	ChabaCooText(SPluginInterface *owner = nullptr, QWidget *parent = nullptr);
	virtual ~ChabaCooText() override = default;

	// TextEditorWidget
	virtual bool isModified() const noexcept override { return false; }

public slots:
	// SGraphicalWidget
	virtual void updateUi(const QString &pluginName) override;

private:
	QString clipboardMimeType() const;
	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);
};

#endif // CHABACOOTEXT_HPP
