/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAOUTPUTTEXT_HPP
#define CHABAOUTPUTTEXT_HPP

#include <editors/text/TextEditorWidget.hpp>

class QMimeData;
struct ChabaMeasures;

/**
 * Text editor for output points in Chaba.
 *
 * This class uses ChabaOutputLexer.
 */
class ChabaOutputText : public TextEditorWidget
{
	Q_OBJECT

public:
	ChabaOutputText(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~ChabaOutputText() override = default;

	// TextEditorWidget
	virtual bool isModified() const noexcept override { return false; }

	// ChabaOutputText
	/**
	 * Uses the input measures to create links between input and output file.
	 *
	 * @warning This method must be called before opening the output file.
	 */
	void chabaMeasures(const ChabaMeasures &measures);
	const ChabaMeasures &chabaMeasures();

public slots:
	// SGraphicalWidget
	virtual void updateUi(const QString &pluginName) override;

private:
	QString clipboardMimeType() const;
	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);
};

#endif // CHABAOUTPUTTEXT_HPP
