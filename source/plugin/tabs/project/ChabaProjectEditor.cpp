#include "ChabaProjectEditor.hpp"

#include <QMessageBox>
#include <QTabWidget>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <utils/SettingsManager.hpp>

#include "ChabaPlugin.hpp"
#include "tabs/project/ChabaProjectGui.hpp"
#include "tabs/project/ChabaProjectText.hpp"
#include "trinity/ChabaConfig.hpp"

class ChabaProjectEditor::_ChabaProjectEditor_pimpl
{
public:
	/** text */
	ChabaProjectText *textEditor = nullptr;
	/** gui */
	ChabaProjectGui *guiEditor = nullptr;
	/** current widget */
	ChabaProjectEditorBase *currentTab = nullptr;
};

ChabaProjectEditor::ChabaProjectEditor(SPluginInterface *owner, QWidget *parent) :
	STabInterface(owner, parent), ChabaProjectEditorBase(), _pimpl(std::make_unique<_ChabaProjectEditor_pimpl>())
{
	enablefileWatcher(true);

	auto config = SettingsManager::settings().settings<ChabaConfigObject>(ChabaPlugin::_name());
	if (config.showTextEditors)
	{
		_pimpl->textEditor = new ChabaProjectText(owner, this);
		_pimpl->textEditor->setWindowTitle(tr("Text editor"));
		installEventFilterAll(this, _pimpl->textEditor);
		tab()->addTab(_pimpl->textEditor, _pimpl->textEditor->windowTitle());
	}
	if (config.showGUIEditors)
	{
		_pimpl->guiEditor = new ChabaProjectGui(owner, this);
		installEventFilterAll(this, _pimpl->guiEditor);
		tab()->addTab(_pimpl->guiEditor, _pimpl->guiEditor->windowTitle());
	}

	_pimpl->currentTab = dynamic_cast<ChabaProjectEditorBase *>(tab()->currentWidget());
}

ChabaProjectEditor::~ChabaProjectEditor() = default;

ChabaProject ChabaProjectEditor::getProject() const
{
	if (!_pimpl->currentTab)
		return ChabaProject();
	ChabaProject project = _pimpl->currentTab->getProject();
	if (const auto *plugin = dynamic_cast<const ChabaPlugin *>(owner()))
	{
		const auto *chproject = plugin->_getProject(this);
		if (chproject)
		{
			project.activePoints = chproject->activePoints;
			project.passivePoints = chproject->passivePoints;
		}
	}
	return project;
}

void ChabaProjectEditor::setProject(const ChabaProject &proj)
{
	if (_pimpl->currentTab)
		_pimpl->currentTab->setProject(proj);
}

void ChabaProjectEditor::tabChanged(int pos)
{
	auto *newtab = dynamic_cast<ChabaProjectEditorBase *>(tab()->widget(pos));
	if (newtab && _pimpl->currentTab && newtab != _pimpl->currentTab)
	{
		try
		{
			newtab->setProject(_pimpl->currentTab->getProject());
		}
		catch (const SPIOException &e)
		{
			tab()->setCurrentWidget(dynamic_cast<QWidget *>(_pimpl->currentTab));
			logCritical() << tr("Can't read contents") << '\n'
						  << tr("Your current file is not valid as it is not possible to read it correctly. If you change the tab now, you may lose everything. Please "
								"fix this first.");
			logDebug() << e.what() << e.contents();
			return;
		}
	}

	STabInterface::tabChanged(pos);
	_pimpl->currentTab = dynamic_cast<ChabaProjectEditorBase *>(tab()->currentWidget());
}
