#include "ChabaProjectGui.hpp"
#include "ui_ChabaProjectGui.h"

#include <QFileInfo>
#include <QTextStream>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>

#include "ChabaProject.hpp"
#include "tabs/project/ChabaProjectLexer.hpp"

ChabaProjectGui::ChabaProjectGui(SPluginInterface *owner, QWidget *parent) :
	SGraphicalWidget(owner, parent), ChabaProjectEditorBase(), ui(std::make_unique<Ui::ChabaProjectGui>())
{
	delete layout();
	ui->setupUi(this);

	// Ensuring dot as decimal place
	auto locale = QLocale(QLocale::C);
	ui->spinTolerance->setLocale(locale);
	ui->spinScale->setLocale(locale);
	ui->spinRotX->setLocale(locale);
	ui->spinRotY->setLocale(locale);
	ui->spinRotZ->setLocale(locale);
	ui->spinTrX->setLocale(locale);
	ui->spinTrY->setLocale(locale);
	ui->spinTrZ->setLocale(locale);
}

ChabaProjectGui::~ChabaProjectGui() = default;

ShareablePointsList ChabaProjectGui::getContent() const
{
	return ShareablePointsList();
}

ChabaProject ChabaProjectGui::getProject() const
{
	ChabaProject proj;
	// global
	proj.title = ui->editTitle->text().toStdString();
	proj.precision = (size_t)ui->spinPrecision->value();
	proj.tolerance = ui->spinTolerance->value();
	proj.maxIterations = ui->spinMaxIterations->value();
	// parameters
	proj.scale = {!ui->checkScale->isChecked(), ui->spinScale->value()};
	proj.trax = {!ui->checkTrX->isChecked(), ui->spinTrX->value()};
	proj.tray = {!ui->checkTrY->isChecked(), ui->spinTrY->value()};
	proj.traz = {!ui->checkTrZ->isChecked(), ui->spinTrZ->value()};
	proj.rotx = {!ui->checkRotX->isChecked(), ui->spinRotX->value()};
	proj.roty = {!ui->checkRotY->isChecked(), ui->spinRotY->value()};
	proj.rotz = {!ui->checkRotZ->isChecked(), ui->spinRotZ->value()};
	// files
	proj.activeFile = ui->editActive->text().toStdString();
	proj.passiveFile = ui->editPassive->text().toStdString();
	proj.outFile = ui->editResult->text().toStdString();
	proj.cooFile = ui->editCoo->text().toStdString();
	proj.punchFile = ui->editPunch->text().toStdString();

	return proj;
}

void ChabaProjectGui::setProject(const ChabaProject &proj)
{
	// global
	ui->editTitle->setText(QString::fromStdString(proj.title));
	ui->spinPrecision->setValue((int)proj.precision);
	ui->spinTolerance->setValue(proj.tolerance);
	ui->spinMaxIterations->setValue(proj.maxIterations);
	// parameters
	ui->checkScale->setChecked(!proj.scale.variable);
	ui->spinScale->setValue(proj.scale.value);
	ui->checkTrX->setChecked(!proj.trax.variable);
	ui->spinTrX->setValue(proj.trax.value);
	ui->checkTrY->setChecked(!proj.tray.variable);
	ui->spinTrY->setValue(proj.tray.value);
	ui->checkTrZ->setChecked(!proj.traz.variable);
	ui->spinTrZ->setValue(proj.traz.value);
	ui->checkRotX->setChecked(!proj.rotx.variable);
	ui->spinRotX->setValue(proj.rotx.value);
	ui->checkRotY->setChecked(!proj.roty.variable);
	ui->spinRotY->setValue(proj.roty.value);
	ui->checkRotZ->setChecked(!proj.rotz.variable);
	ui->spinRotZ->setValue(proj.rotz.value);
	// files
	ui->editActive->setText(QString::fromStdString(proj.activeFile));
	ui->editPassive->setText(QString::fromStdString(proj.passiveFile));
	ui->editResult->setText(QString::fromStdString(proj.outFile));
	ui->editCoo->setText(QString::fromStdString(proj.cooFile));
	ui->editPunch->setText(QString::fromStdString(proj.punchFile));
}

void ChabaProjectGui::clearContent()
{
	SGraphicalWidget::clearContent();
	ChabaProject proj;
	setProject(proj);
}

void ChabaProjectGui::retranslate()
{
	SGraphicalWidget::retranslate();
	ui->retranslateUi(this);
}

void ChabaProjectGui::precisionChanged(int prec)
{
	ui->spinTolerance->setDecimals(prec);
	ui->spinScale->setDecimals(prec);
	ui->spinTrX->setDecimals(prec);
	ui->spinTrY->setDecimals(prec);
	ui->spinTrZ->setDecimals(prec);
	ui->spinRotX->setDecimals(prec);
	ui->spinRotY->setDecimals(prec);
	ui->spinRotZ->setDecimals(prec);
}

void ChabaProjectGui::resultFileChanged(const QString &text)
{
	ui->editLog->setText(QFileInfo(text).baseName() + "Log.log");
}

bool ChabaProjectGui::_save(const QString &path)
{
	QFile file(path);
	if (!file.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::OpenModeFlag::Truncate | QIODevice::OpenModeFlag::Text))
		return false;
	ChabaProjectLexer lexer;
	QTextStream stream(&file);
	stream.setCodec("UTF-8");
	stream << QString::fromStdString(lexer.write(getProject()));
	file.close();
	return true;
}

bool ChabaProjectGui::_open(const QString &path)
{
	QFile file(path);
	if (!file.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::OpenModeFlag::Text))
		return false;
	ChabaProjectLexer lexer;
	QTextStream stream(&file);
	stream.setCodec("UTF-8");
	QString contents = stream.readAll();
	file.close();
	try
	{
		setProject(lexer.read(contents.toStdString()));
	}
	catch (...)
	{
		return false;
	}
	return true;
}

void ChabaProjectGui::_newEmpty()
{
	ChabaProject project;
	project.title = "Title-Example";
	project.activeFile = "active.act";
	project.passiveFile = "passive.pas";
	project.outFile = "result.out";
	project.cooFile = "coofile.coo";
	project.punchFile = "punchfile.pun";
	setProject(project);
}
