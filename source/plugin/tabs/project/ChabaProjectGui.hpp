/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPROJECTGUI_HPP
#define CHABAPROJECTGUI_HPP

#include <interface/SGraphicalInterface.hpp>

#include "tabs/project/ChabaProjectEditorBase.hpp"

class ChabaProject;

namespace Ui
{
class ChabaProjectGui;
}

class ChabaProjectGui : public SGraphicalWidget, public ChabaProjectEditorBase
{
	Q_OBJECT

public:
	ChabaProjectGui(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~ChabaProjectGui() override;

	// SGraphicalWidget
	virtual ShareablePointsList getContent() const override;

	// ChabaProjectEditorBase
	virtual ChabaProject getProject() const override;
	virtual void setProject(const ChabaProject &proj) override;

public slots:
	// SGraphicalWidget
	virtual void clearContent() override;
	virtual void retranslate() override;
	// ChabaProjectGui
	void precisionChanged(int prec);
	void resultFileChanged(const QString &text);

protected:
	// SGraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

private slots:
	void changesMade()
	{
		isModified(true);
		emit hasChanged();
	};

private:
	std::unique_ptr<Ui::ChabaProjectGui> ui;
};

#endif // CHABAPROJECTGUI_HPP
