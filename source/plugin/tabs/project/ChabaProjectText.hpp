/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPROJECTTEXT_HPP
#define CHABAPROJECTTEXT_HPP

#include <editors/text/TextEditorWidget.hpp>

#include "tabs/project/ChabaProjectEditorBase.hpp"

class ChabaProject;

/**
 * Text editor for project files
 *
 * This class uses ChabaProjectLexer for styling.
 */
class ChabaProjectText : public TextEditorWidget, public ChabaProjectEditorBase
{
	Q_OBJECT

public:
	ChabaProjectText(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~ChabaProjectText() override = default;

	// ChabaProjectEditorBase
	virtual ChabaProject getProject() const override;
	virtual void setProject(const ChabaProject &proj) override;

public slots:
	// SGraphicalWidget
	virtual void updateUi(const QString &pluginName) override;

protected:
	// SGraphicalWidget
	virtual void _newEmpty() override;
};

#endif // CHABAPROJECTTEXT_HPP
