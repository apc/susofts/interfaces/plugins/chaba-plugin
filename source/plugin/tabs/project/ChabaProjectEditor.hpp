/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPROJECTEDITOR_HPP
#define CHABAPROJECTEDITOR_HPP

#include <memory>

#include <interface/STabInterface.hpp>

#include "tabs/project/ChabaProjectEditorBase.hpp"

/**
 * Project editor tab.
 *
 * This editor is composed of 2 tabs:
 * - a text editor with ChabaProjectText
 * - a UI editor with ChabaProjectGui
 */
class ChabaProjectEditor : public STabInterface, public ChabaProjectEditorBase
{
	Q_OBJECT

public:
	ChabaProjectEditor(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~ChabaProjectEditor() override;

	// ChabaProjectEditorBase
	virtual ChabaProject getProject() const override;
	virtual void setProject(const ChabaProject &proj) override;

protected slots:
	virtual void tabChanged(int pos) override;

private:
	/** pimpl */
	class _ChabaProjectEditor_pimpl;
	std::unique_ptr<_ChabaProjectEditor_pimpl> _pimpl;
};

#endif // CHABAPROJECTEDITOR_HPP
