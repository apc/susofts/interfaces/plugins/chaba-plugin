/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPROJECTLEXER_HPP
#define CHABAPROJECTLEXER_HPP

#include <editors/text/TextLexer.hpp>

#include "ChabaProject.hpp"

class ChabaPlugin;

/**
 * Lexer for Chaba project file.
 *
 * Note that this class can also be used as a serializer.
 */
class ChabaProjectLexer : public TextLexer
{
	Q_OBJECT

public:
	ChabaProjectLexer(QObject *parent = nullptr);
	virtual ~ChabaProjectLexer() override = default;

	// QsciLexerCustom
	enum Styles
	{
		/** Default */
		Default,
		/** Comment / ignored part */
		Comment,
		/** Precision line */
		Precision,
		/** Title line */
		Title,
		/** Tolerance line */
		Tolerance,
		/** MaxIterations line */
		MaxIterations,
		/** Scale, translation and rotation settings */
		Settings,
		/** Input file line */
		InputFile,
		/** Output file line */
		OutputFile,
		/** End of enum */
		MaxStyle,
		/** Flag for keywords */
		Keyword = 0b10000, // 16
	};
	virtual const char *language() const override { return "Chaba Project"; }
	virtual const char *keywords(int set) const override;
	virtual QString description(int style) const override;
	virtual const char *wordCharacters() const override;
	virtual bool defaultEolFill(int style) const override;
	QColor defaultColor(int style) const override;
	QFont defaultFont(int style) const override;
	QColor defaultPaper(int style) const override;
	// actually only used for autocompletion
	virtual QStringList autoCompletionWordSeparators() const override { return {" ", "\t"}; }

	// ChabaProjectLexer
	/** Read the content of the string and fill this class with the values */
	ChabaProject read(const std::string &projectfile);
	/** write the content of this class to the return string, in the correct format */
	std::string write(const ChabaProject &project);

protected:
	// TextLexer
	virtual void styleText() override;
	virtual bool startLexer(int, int) override;
	virtual void endLexer() override;

private:
	/** Analyze and style one line */
	void readLine(std::string line);
	/** Read precision */
	void readPrec(std::string line);
	/** read title */
	void readTitle(std::string line);
	/** read tolerance */
	void readTolerance(std::string line);
	/** read tolerance */
	void readMaxIterations(std::string line);
	/** read settings */
	void readSettings(std::string line);
	/** read file */
	void readFile(std::string line);
	/** read comment */
	void readComment(std::string line);

private:
	/** tell if we are in lexer mode */
	bool _lexerMode = false;
	/** project */
	ChabaProject _project;
};

#endif // CHABAPROJECTLEXER_HPP
