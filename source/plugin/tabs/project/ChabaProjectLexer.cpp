#include "ChabaProjectLexer.hpp"

#include <iomanip>
#include <sstream>

#include <QSet>

#include <Qsci/qsciabstractapis.h>
#include <Qsci/qsciscintilla.h>

#include <ShareablePoints/SPIOException.hpp>
#include <StringManager.h>
#include <editors/text/TextUtils.hpp>

#include "ChabaPlugin.hpp"

namespace
{
const QString HELP_STRING = QObject::tr(R"""(
	<h2>Project file</h2>
	<p>The project file define type of calculation, the Helmert parameters and gives access to the data set.</p>
	<h3>Comments</h3>
	<p>You can add as many comment lines as you want. Simply start the line with a "<code>&#35;</code>".</p>
	<h3>Example</h3>
	<pre>
	*PREC  n
	#projectFileVersion   5
	title  dirk
	tolerance   0.5
	maxIterations 50
	scale   variable|fixed    1.0
	TrX   variable|fixed   0.0000000
	TrY   variable|fixed   0.0000000
	TrZ   variable|fixed   0.0000000
	RotX   variable|fixed   0.0000000
	RotY   variable|fixed   0.0000000
	RotZ   variable|fixed   0.0000000
	passiveFile   name.pas
	activeFile   name.act
	cooFile   name.coo
	punchFile   name.pun
	resultFile   name.out
	</pre>
	<h3>Documentation</h3>
	<p>Further documentation can be found <a href="https://confluence.cern.ch/display/SUS/Chaba+V5_UserGuide#ChabaV5_UserGuide-_Toc4644780261.Projectfile">here</a>.</p>
)""");
const QString COMMENT_STRING = QObject::tr(R"""(<h3>Comment</h3><p>This is a comment, as the line starts with the character "<code>&#35;</code>".</p>)""");
const QString TITLE_STRING = QObject::tr(R"""(<h3>Title</h3><p>The title of the calculation</p>)""");
const QString _VARIABLE_STRING = QObject::tr(R"""(
	<dl>
		<dt><code>variable</code></dt><dd>implies the %1 is to be calculated;</dd>
		<dt><code>fixed</code></dt><dd>sets the %1 equal to the value that follows.</dd>
	</dl>
	<p>If it's not informed, it will be set to <code>variable</code> and then saved into the project file.</p>
)""");
const QString _SETTINGS_STRING = QObject::tr(R"""(<h3>%1</h3><p>This parameter fixes a provisional value of %1 before the calculation.</p>)""");
const QString SCALE_STRING = _SETTINGS_STRING.arg(QObject::tr("the scale parameter")) + _VARIABLE_STRING.arg(QObject::tr("scale"));
const QString TRX_STRING = _SETTINGS_STRING.arg(QObject::tr("translation on X axis")) + _VARIABLE_STRING.arg(QObject::tr("translation"));
const QString TRY_STRING = _SETTINGS_STRING.arg(QObject::tr("translation on Y axis")) + _VARIABLE_STRING.arg(QObject::tr("translation"));
const QString TRZ_STRING = _SETTINGS_STRING.arg(QObject::tr("translation on Z axis")) + _VARIABLE_STRING.arg(QObject::tr("translation"));
const QString ROTX_STRING = _SETTINGS_STRING.arg(QObject::tr("rotation around X axis (omega angle)")) + _VARIABLE_STRING.arg(QObject::tr("rotation"));
const QString ROTY_STRING = _SETTINGS_STRING.arg(QObject::tr("rotation around Y axis (phi angle)")) + _VARIABLE_STRING.arg(QObject::tr("rotation"));
const QString ROTZ_STRING = _SETTINGS_STRING.arg(QObject::tr("rotation around Z axis (kappa angle)")) + _VARIABLE_STRING.arg(QObject::tr("rotation"));
const QString TOLERANCE_STRING = QObject::tr(R"""(<h3>Tolerance</h3>
	<p>The criteria applied to the residual for a given point to decide if it should be removed from the list of points used for determining the transformation parameters.</p>)""");
const QString MAXITERATIONS_STRING = QObject::tr(R"""(<h3>MaxIterations</h3>
	<p>The maximal number of best-fit iterations before the algorithm is stopped. The algorithm can be stopped prematurely if the convertgence criteria is achieved earlier.</p>)""");
const QString PRECISION_STRING = QObject::tr(R"""(<h3>Precision</h3>
	<p>The number of decimal places (for a length meter) to be used in the output files. May be a value from 1 to 7.
	<br /><em>This keyword is optional</em>. The default value is 5.</p>)""");
const QString ACTIVEFILE_STRING = QObject::tr(R"""(<h3>Active file</h3><p>The path of the active (reference) point data file.</p>)""");
const QString PASSIVEFILE_STRING = QObject::tr(R"""(<h3>Passive file</h3><p>The path of the passive point data file.</p>)""");
const QString RESULTFILE_STRING = QObject::tr(R"""(<h3>Result file</h3>
	<p>The path of the results data file.
	<br /><em>This keyword is optional</em>. If it's not provided, an automatic name will be chosen for the output file.</p>)""");
const QString COOFILE_STRING = QObject::tr(R"""(<h3>Coo file</h3>
	<p>The path of the summary results point data file for insertion into the SURVEY database.
	<br /><em>This keyword is optional</em>. If it is not provided, no coo file will be written.</p>)""");
const QString PUNCHFILE_STRING = QObject::tr(R"""(<h3>Punch file</h3>
	<p>The path of the summary results point data file.
	<br /><em>This keyword is optional</em>. If it is not provided, no punch file will be written.</p>)""");

inline int getPosition(std::istringstream &sstr)
{
	return sstr.eof() ? (int)sstr.str().size() : (int)sstr.tellg();
}

template<class T>
inline bool readValue(std::istringstream &sstr, T &value)
{
	sstr >> value;
	return !sstr.fail();
}

class ChabaProjectLexerAPIs : public QsciAbstractAPIs
{
public:
	ChabaProjectLexerAPIs(QsciLexer *lexer) :
		QsciAbstractAPIs(lexer), _settingsNames(QSet<QString>::fromList(QString(lexer->keywords(ChabaProjectLexer::Styles::Settings)).split(' ')))
	{
		for (int i = ChabaProjectLexer::Styles::Default; i < ChabaProjectLexer::Styles::MaxStyle; i++)
		{
			const char *keywords = lexer->keywords(i);
			if (!keywords)
				continue;
			_apis += QString(keywords).split(' ');
		}
	}

	// QsciAbstractAPIs
	virtual void updateAutoCompletionList(const QStringList &context, QStringList &list) override
	{
		if (context.size() > 2)
			return;

		QStringList *apis = &_apis;
		QStringRef beginning;

		if (!context.isEmpty())
			beginning = &context.last();
		if (context.size() == 2)
		{
			if (_settingsNames.contains(context.first()))
				apis = &_settingsApi;
			else
				return;
		}

		apis->sort(toQtCaseSensitivity());
		list.reserve(apis->size());
		for (const auto &api : *apis)
		{
			if (api.compare(beginning, toQtCaseSensitivity()) < 0)
				continue;
			else if (api.startsWith(beginning, toQtCaseSensitivity()))
				list.append(api);
			else
				break;
		}
	}
	virtual QStringList callTips(const QStringList &, int, QsciScintilla::CallTipsStyle, QList<int> &) override { return QStringList(); }

private:
	Qt::CaseSensitivity toQtCaseSensitivity() { return lexer()->caseSensitive() ? Qt::CaseSensitivity::CaseSensitive : Qt::CaseSensitivity::CaseInsensitive; }

private:
	const QSet<QString> _settingsNames;
	QStringList _apis;
	QStringList _settingsApi = {"fixed", "variable"};
};
} // namespace

ChabaProjectLexer::ChabaProjectLexer(QObject *parent) : TextLexer(parent)
{
	// set APIs for autocompletion
	setAPIs(new ChabaProjectLexerAPIs(this));
}

const char *ChabaProjectLexer::keywords(int set) const
{
	set &= ~Styles::Keyword;

	switch (set)
	{
	case Styles::Comment:
		return "#";
	case Styles::Precision:
		return "*PREC";
	case Styles::Title:
		return "title";
	case Styles::Tolerance:
		return "tolerance";
	case Styles::MaxIterations:
		return "maxIterations";
	case Styles::Settings:
		return "scale TrX TrY TrZ RotX RotY RotZ";
	case Styles::InputFile:
		return "activeFile passiveFile";
	case Styles::OutputFile:
		return "resultFile cooFile punchFile";
	}

	return nullptr;
}

QString ChabaProjectLexer::description(int style) const
{
	style &= ~Styles::Keyword;

	switch (style)
	{
	case Styles::Default:
		return "Default";
	case Styles::Comment:
		return "Comment";
	case Styles::Precision:
		return "Precision";
	case Styles::Title:
		return "Title";
	case Styles::Tolerance:
		return "Tolerance";
	case Styles::MaxIterations:
		return "MaxIterations";
	case Styles::Settings:
		return "Settings for scale, translations and rotations";
	case Styles::InputFile:
		return "Input files (active/ref and passive)";
	case Styles::OutputFile:
		return "Output files (.out, .coo, .pun)";
	}

	return QString();
}

const char *ChabaProjectLexer::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-*éèà";
}

bool ChabaProjectLexer::defaultEolFill(int) const
{
	return false;
}

QColor ChabaProjectLexer::defaultColor(int style) const
{
	style &= ~Styles::Keyword;

	switch (style)
	{
	case Styles::Default:
		return Qt::GlobalColor::darkGray;
	case Styles::Comment:
		return Qt::GlobalColor::darkGreen;
	case Styles::Precision:
	case Styles::Title:
		return Qt::GlobalColor::black;
	case Styles::Tolerance:
	case Styles::MaxIterations:
		return Qt::GlobalColor::magenta;
	case Styles::Settings:
		return Qt::GlobalColor::darkMagenta;
	case Styles::InputFile:
		return Qt::GlobalColor::darkCyan;
	case Styles::OutputFile:
		return Qt::GlobalColor::darkBlue;
	}

	return QsciLexerCustom::defaultColor(style);
}

QFont ChabaProjectLexer::defaultFont(int style) const
{
	bool bold = false;

	if (style & Styles::Keyword)
	{
		bold = true;
		style &= ~Styles::Keyword;
	}
	if (style >= Styles::MaxStyle)
		return QsciLexerCustom::defaultFont(style);

	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	if (bold)
		baseFont.setWeight(QFont::Weight::Black);
	else
		baseFont.setWeight(QFont::Weight::Light);

	return baseFont;
}

QColor ChabaProjectLexer::defaultPaper(int style) const
{
	return QsciLexerCustom::defaultPaper(style);
}

ChabaProject ChabaProjectLexer::read(const std::string &projectfile)
{
	_project = ChabaProject();
	std::istringstream sstr(projectfile);
	std::string line;

	while (!std::getline(sstr, line).fail())
	{
		if (line.empty())
			continue;
		readLine(std::move(line));
	}
	return _project;
}

std::string ChabaProjectLexer::write(const ChabaProject &project)
{
	std::ostringstream sstr;

	sstr << std::fixed << std::setprecision(project.precision);

	sstr << "*PREC" << '\t' << project.precision << '\n';
	sstr << "#projectFileVersion" << '\t' << "5\n";
	sstr << "title" << '\t' << project.title << '\n';
	sstr << '\n';
	sstr << "tolerance" << '\t' << project.tolerance << '\n';
	sstr << "maxIterations" << '\t' << project.maxIterations << '\n';
	sstr << '\n';
	sstr << "scale" << '\t' << (project.scale.variable ? "variable" : "fixed") << '\t' << project.scale.value << '\n';
	sstr << "TrX" << '\t' << (project.trax.variable ? "variable" : "fixed") << '\t' << project.trax.value << '\n';
	sstr << "TrY" << '\t' << (project.tray.variable ? "variable" : "fixed") << '\t' << project.tray.value << '\n';
	sstr << "TrZ" << '\t' << (project.traz.variable ? "variable" : "fixed") << '\t' << project.traz.value << '\n';
	sstr << "RotX" << '\t' << (project.rotx.variable ? "variable" : "fixed") << '\t' << project.rotx.value << '\n';
	sstr << "RotY" << '\t' << (project.roty.variable ? "variable" : "fixed") << '\t' << project.roty.value << '\n';
	sstr << "RotZ" << '\t' << (project.rotz.variable ? "variable" : "fixed") << '\t' << project.rotz.value << '\n';
	sstr << '\n';
	sstr << "activeFile" << '\t' << project.activeFile << '\n';
	sstr << "passiveFile" << '\t' << project.passiveFile << '\n';
	sstr << '\n';
	sstr << "resultFile" << '\t' << project.outFile << '\n';
	if (!project.cooFile.empty())
		sstr << "cooFile" << '\t' << project.cooFile << '\n';
	if (!project.punchFile.empty())
		sstr << "punchFile" << '\t' << project.punchFile << '\n';

	return sstr.str();
}

void ChabaProjectLexer::styleText()
{
	try
	{
		readLine(toByteArray(_currentLine.toString(), editor()).constData());
	}
	catch (...)
	{
		addMarker(editor(), _curpos, 0);
	}
}

bool ChabaProjectLexer::startLexer(int, int)
{
	_lexerMode = true;
	if (!editor())
		return false;
	// trick to always analyse the whole text
	// if we see that analyse become too slow, we can try to improve the lexer here
	// this is needed to keep the sets of active and passive points up to date
	return TextLexer::startLexer(0, editor()->length());
}

void ChabaProjectLexer::endLexer()
{
	_lexerMode = false;
}

void ChabaProjectLexer::readLine(std::string line)
{
	if (line.empty())
	{
		_infoAnnotation = HELP_STRING;
		return;
	}

	if (trim(line)[0] == '#')
	{
		_infoAnnotation = COMMENT_STRING;
		readComment(std::move(line));
	}
	else if (line.compare(0, 5, "*PREC") == 0)
	{
		_infoAnnotation = PRECISION_STRING;
		readPrec(std::move(line));
	}
	else if (line.compare(0, 5, "title") == 0)
	{
		_infoAnnotation = TITLE_STRING;
		readTitle(std::move(line));
	}
	else if (line.compare(0, 9, "tolerance") == 0)
	{
		_infoAnnotation = TOLERANCE_STRING;
		readTolerance(std::move(line));
	}
	else if (line.compare(0, 13, "maxIterations") == 0)
	{
		_infoAnnotation = MAXITERATIONS_STRING;
		readMaxIterations(std::move(line));
	}
	else if (line.compare(0, 5, "scale") == 0)
	{
		_infoAnnotation = SCALE_STRING;
		readSettings(std::move(line));
	}
	else if (line.compare(0, 3, "TrX") == 0)
	{
		_infoAnnotation = TRX_STRING;
		readSettings(std::move(line));
	}
	else if (line.compare(0, 3, "TrY") == 0)
	{
		_infoAnnotation = TRY_STRING;
		readSettings(std::move(line));
	}
	else if (line.compare(0, 3, "TrZ") == 0)
	{
		_infoAnnotation = TRZ_STRING;
		readSettings(std::move(line));
	}
	else if (line.compare(0, 4, "RotX") == 0)
	{
		_infoAnnotation = ROTX_STRING;
		readSettings(std::move(line));
	}
	else if (line.compare(0, 4, "RotY") == 0)
	{
		_infoAnnotation = ROTY_STRING;
		readSettings(std::move(line));
	}
	else if (line.compare(0, 4, "RotZ") == 0)
	{
		_infoAnnotation = ROTZ_STRING;
		readSettings(std::move(line));
	}
	else if (line.compare(0, 10, "activeFile") == 0)
	{
		_infoAnnotation = ACTIVEFILE_STRING;
		readFile(std::move(line));
	}
	else if (line.compare(0, 11, "passiveFile") == 0)
	{
		_infoAnnotation = PASSIVEFILE_STRING;
		readFile(std::move(line));
	}
	else if (line.compare(0, 10, "resultFile") == 0)
	{
		_infoAnnotation = RESULTFILE_STRING;
		readFile(std::move(line));
	}
	else if (line.compare(0, 7, "cooFile") == 0)
	{
		_infoAnnotation = COOFILE_STRING;
		readFile(std::move(line));
	}
	else if (line.compare(0, 9, "punchFile") == 0)
	{
		_infoAnnotation = PUNCHFILE_STRING;
		readFile(std::move(line));
	}
	else if (_lexerMode)
	{
		_infoAnnotation = HELP_STRING;
	}
}

void ChabaProjectLexer::readPrec(std::string line)
{
	if (line.compare(0, 5, "*PREC") != 0)
		throw SPIOException(std::string("Can't convert to a precision"), "", line);
	std::string tmp;
	std::istringstream sstr(line);
	if (!readValue(sstr, tmp) && tmp != "*PREC")
		throw SPIOException(std::string("Can't convert to a precision"), "", line);

	size_t prec;
	if (!readValue(sstr, prec))
		throw SPIOException(std::string("Can't convert to a precision"), "", line);

	_project.precision = prec;
	if (_lexerMode)
	{
		addStyle(5, Styles::Precision | Styles::Keyword);
		addStyle(getPosition(sstr) - 5, Styles::Precision);
	}
}

void ChabaProjectLexer::readTitle(std::string line)
{
	if (line.compare(0, 5, "title") != 0)
		throw SPIOException(std::string("Can't convert to a title"), "", line);
	std::string key, title;
	std::istringstream sstr(line);

	sstr >> key;
	std::getline(sstr, title);
	title = trim(std::move(title));
	if (sstr.fail() || key.empty() || title.empty())
		throw SPIOException(std::string("Can't convert to a title"), "", line);

	_project.title = title;
	if (_lexerMode)
	{
		addStyle(5, Styles::Title | Styles::Keyword);
		addStyle(getPosition(sstr) - 5, Styles::Title);
	}
}

void ChabaProjectLexer::readTolerance(std::string line)
{
	if (line.compare(0, 9, "tolerance") != 0)
		throw SPIOException(std::string("Can't convert to a tolerance"), "", line);
	std::string tmp;
	std::istringstream sstr(line);
	if (!readValue(sstr, tmp) && tmp != "tolerance")
		throw SPIOException(std::string("Can't convert to a tolerance"), "", line);

	double tol;
	if (!readValue(sstr, tol))
		throw SPIOException(std::string("Can't convert to a tolerance"), "", line);

	_project.tolerance = tol;
	if (_lexerMode)
	{
		addStyle(9, Styles::Tolerance | Styles::Keyword);
		addStyle(getPosition(sstr) - 9, Styles::Tolerance);
	}
}

void ChabaProjectLexer::readMaxIterations(std::string line)
{
	if (line.compare(0, 13, "maxIterations") != 0)
		throw SPIOException(std::string("Can't convert to maxIterations number"), "", line);
	std::string tmp;
	std::istringstream sstr(line);
	if (!readValue(sstr, tmp) && tmp != "maxIterations")
		throw SPIOException(std::string("Can't convert to maxIterations number"), "", line);

	int maxiter;
	if (!readValue(sstr, maxiter))
		throw SPIOException(std::string("Can't convert to maxIterations number"), "", line);
	else if (maxiter < 1)
		throw SPIOException(std::string("Number of iterations must be greater than 0"), "", line);

	_project.maxIterations = maxiter;
	if (_lexerMode)
	{
		addStyle(13, Styles::MaxIterations | Styles::Keyword);
		addStyle(getPosition(sstr) - 13, Styles::MaxIterations);
	}
}

void ChabaProjectLexer::readSettings(std::string line)
{
	std::istringstream sstr(line);
	std::string key, variable;
	double value;

	sstr >> key >> variable >> value;
	if (sstr.fail() || key.empty() || variable.empty() || line.compare(0, key.size(), key) != 0 || (variable != "variable" && variable != "fixed"))
		throw SPIOException(std::string("Can't convert to a transformation parameter: '" + key + "'"), "", line);

	if (key == "scale")
		_project.scale = {(variable == "variable"), value};
	else if (key == "TrX")
		_project.trax = {(variable == "variable"), value};
	else if (key == "TrY")
		_project.tray = {(variable == "variable"), value};
	else if (key == "TrZ")
		_project.traz = {(variable == "variable"), value};
	else if (key == "RotX")
		_project.rotx = {(variable == "variable"), value};
	else if (key == "RotY")
		_project.roty = {(variable == "variable"), value};
	else if (key == "RotZ")
		_project.rotz = {(variable == "variable"), value};
	else
		throw SPIOException(std::string("Can't convert to a transformation parameter: '" + key + "'"), "", line);

	if (_lexerMode)
	{
		addStyle((int)key.size(), Styles::Settings | Styles::Keyword);
		addStyle(getPosition(sstr) - (int)key.size(), Styles::Settings);
	}
}

void ChabaProjectLexer::readFile(std::string line)
{
	std::istringstream sstr(line);
	std::string key, file;

	sstr >> key >> std::quoted(file);
	if (sstr.fail() || key.empty() || file.empty() || line.compare(0, key.size(), key) != 0 || file.find('"') != std::string::npos)
		throw SPIOException(std::string("Can't convert to a file: '" + key + "'"), "", line);

	Styles style;
	if (key == "activeFile")
	{
		style = Styles::InputFile;
		_project.activeFile = file;
	}
	else if (key == "passiveFile")
	{
		style = Styles::InputFile;
		_project.passiveFile = file;
	}
	else if (key == "resultFile")
	{
		style = Styles::OutputFile;
		_project.outFile = file;
	}
	else if (key == "cooFile")
	{
		style = Styles::OutputFile;
		_project.cooFile = file;
	}
	else if (key == "punchFile")
	{
		style = Styles::OutputFile;
		_project.punchFile = file;
	}
	else
		throw SPIOException(std::string("Can't convert to a file: '" + key + "'"), "", line);

	if (_lexerMode)
	{
		addStyle((int)key.size(), style | Styles::Keyword);
		addStyle(getPosition(sstr) - (int)key.size(), style);
	}
}

void ChabaProjectLexer::readComment(std::string line)
{
	std::string tmp;
	std::istringstream sstr(line);
	if (!readValue(sstr, tmp) && tmp[0] != '#')
		throw SPIOException(std::string("Can't convert to a comment"), "", line);
	if (_lexerMode)
		addStyle((int)line.size(), Styles::Comment);
}
