/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPROJECTEDITORBASE_HPP
#define CHABAPROJECTEDITORBASE_HPP

#include <QString>

#include "ChabaProject.hpp"

/**
 * small interface for Project editors.
 */
class ChabaProjectEditorBase
{
public:
	/** @return the project */
	virtual ChabaProject getProject() const = 0;
	/** Set the project and update the editor */
	virtual void setProject(const ChabaProject &proj) = 0;
};

#endif // CHABAPROJECTEDITORBASE_HPP
