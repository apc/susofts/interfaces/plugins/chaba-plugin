#include "ChabaProjectText.hpp"

#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <utils/SettingsManager.hpp>

#include "ChabaPlugin.hpp"
#include "tabs/project/ChabaProjectLexer.hpp"
#include "trinity/ChabaConfig.hpp"

ChabaProjectText::ChabaProjectText(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent), ChabaProjectEditorBase()
{
	updateUi(owner->name());
}

ChabaProject ChabaProjectText::getProject() const
{
	return ChabaProjectLexer().read(toByteArray(editor()->text(), editor()).constData());
}

void ChabaProjectText::setProject(const ChabaProject &proj)
{
	editor()->setText(fromByteArray(ChabaProjectLexer().write(proj).c_str(), editor()));
}

void ChabaProjectText::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);
	if (pluginName != ChabaPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<ChabaConfigObject>(pluginName);

	if (!config.color)
		editor()->setLexer(nullptr);
	else if (!editor()->lexer())
		editor()->setLexer(new ChabaProjectLexer(this));
}

void ChabaProjectText::_newEmpty()
{
	ChabaProject project;
	project.title = "Title-Example";
	project.activeFile = "active.act";
	project.passiveFile = "passive.pas";
	project.outFile = "result.out";
	project.cooFile = "coofile.coo";
	project.punchFile = "punchfile.pun";
	setProject(project);
}
