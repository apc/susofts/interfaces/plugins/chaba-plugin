#include "ChabaCooLexer.hpp"

#include <Qsci/qsciscintilla.h>

#include <editors/text/TextUtils.hpp>

#include "ChabaPlugin.hpp"
#include "ChabaProject.hpp"

QString ChabaCooLexer::description(int style) const
{
	style &= ~Styles::PointRef;
	return CooLexerIO::description(style);
}

QColor ChabaCooLexer::defaultColor(int style) const
{
	style &= ~Styles::PointRef;
	return CooLexerIO::defaultColor(style);
}

QFont ChabaCooLexer::defaultFont(int style) const
{
	int s = style & ~Styles::PointRef;
	auto font = CooLexerIO::defaultFont(s);
	if (style & Styles::PointRef)
		font.setWeight(QFont::Weight::Bold);
	return font;
}

bool ChabaCooLexer::startLexer(int start, int end)
{
	if (!editor())
		return false;
	if (_plugin && _plugin->_getProject(this))
	{
		if (_type == Type::Active)
			_plugin->_getProject(this)->activePoints.clear();
		else if (_type == Type::Passive)
			_plugin->_getProject(this)->passivePoints.clear();
	}
	editor()->markerDeleteAll(Marker::Reference);
	return CooLexerIO::startLexer(start, end);
}

void ChabaCooLexer::gotPointName(QStringRef sym)
{
	_refPoint = false;
	bool badPoint = false;
	auto *project = _plugin->_getProject(this);

	if (_lexerMode && project)
	{
		std::string pointName = toByteArray(sym.trimmed().toString(), utf8()).constData();
		if (_type == Type::Active || _type == Type::Passive)
		{
			auto &toinsert = _type == Type::Active ? project->activePoints : project->passivePoints;
			const auto &tocheck = _type == Type::Active ? project->passivePoints : project->activePoints;

			const auto &[it, dontexist] = toinsert.insert(pointName);
			if (!dontexist)
				badPoint = true;
			else
				_refPoint = tocheck.find(pointName) != std::cend(tocheck);
		}
		else
			_refPoint = project->activePoints.find(pointName) != std::end(project->activePoints) && project->passivePoints.find(pointName) != std::end(project->passivePoints);
	}
	if (badPoint)
		addMarker(editor(), sym.position(), TextMarker::ERROR);
	if (_refPoint)
		addMarker(editor(), sym.position(), Marker::Reference);
}

void ChabaCooLexer::addRefStyle(QStringRef sym)
{
	if (!_refPoint)
		return;

	const int position = sym.position() - _currentLine.position();
	auto &styles = _styles.lastLine();
	auto i = std::begin(styles);
	int posbeg = 0;
	// find start insertion position
	for (auto &[len, style] : styles)
	{
		posbeg += len;
		if (posbeg > position)
		{
			style |= Styles::PointRef;
			break;
		}
	}
}
