#include "ChabaPlugin.hpp"

#include <unordered_map>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <interface/SPluginLoader.hpp>
#include <utils/ClipboardManager.hpp>

#include "ChabaProject.hpp"
#include "Version.hpp"
#include "entrypoints/EPplugin_lexers.hpp"
#include "tabs/inputs/ChabaPointLexerIO.hpp"
#include "tabs/outputs/ChabaOutputLexer.hpp"
#include "trinity/ChabaConfig.hpp"
#include "trinity/ChabaGraphicalWidget.hpp"
#include "trinity/ChabaLauncherObject.hpp"

namespace
{
const SGraphicalWidget *findAncestor(const QObject *id)
{
	if (!id)
		return nullptr;
	const SGraphicalWidget *tmp = nullptr, *widget = qobject_cast<const SGraphicalWidget *>(id);
	while (id->parent())
	{
		id = id->parent();
		tmp = qobject_cast<const SGraphicalWidget *>(id);
		if (tmp)
			widget = tmp;
	}
	return widget;
}
} // namespace

class ChabaPlugin::_ChabaPlugin_pimpl
{
public:
	/** list of ChabaProject in the different instances */
	std::unordered_map<const SGraphicalWidget *, ChabaProject> projects;
};

const QString ChabaPlugin::_HELP_STRING = QObject::tr(R"""(
	<h2>%1 file</h2>
	<p>In the new format, sigma can be added to define the point. They are used to fill the weight matrix for the least square process (weight matrix element = <code>1 / sigma<sup>2</sup></code>).
	Older file can be read. Inactive or bad point (name starting with <code>"!"</code> OR <code>"?"</code> <em>ARE EQUIVALENT TO</em> <code>SX 0.0 SY 0.0 SZ 0.0</code>) are considered as fixed point.</p>
	<p>Sigma, <code>SX</code> <code>SY</code> and <code>SZ</code>, are optional, they allowed to use weight on the parameters (<code>1 / sigma<sup>2</sup></code> in the weight matrix for the least square process).</p>
	<h3>Example</h3>
	<p>The different point types:</p>
	<pre>
	 *referencial (OLOC, RS2K, LEP or SPHE)
	 Name   X   Y 
	 Name   X   Y   $DCUM   ID   comments
	 Name   X   Y   SX   value   SY   value
	 Name   X   Y   SX   value   SY   value   $DCUM   ID   comments
	 Name   X   Y   Z
	 Name   X   Y   Z   $DCUM   ID   comments
	 Name   X   Y   Z   SX   value   SY   value   SZ   value
	 Name   X   Y   Z   SX   value   SY   value   SZ   value   $DCUM   ID   comments
	</pre>
	<h3>Documentation</h3>
	<p>Further documentation can be found <a href="https://confluence.cern.ch/display/SUS/ChaBa+V5+User+Guide+-EN">here</a>.</p>
)""");
const QString ChabaPlugin::_REFERENTIAL_STRING = QObject::tr(R"""(<h3>Referential</h3>
	<p>Coordinate system used by the points described in the file.</p>
	<dl>
		<dt><code>*OLOC</code></dt><dd>local and cartesian coordinate system</dd>
		<dt><code>*RS2K</code></dt><dd>geoid centered on the LHC, updated in 2000</dd>
		<dt><code>*LEP</code></dt><dd>old geoid centered on the LHC/LEP, from 1985</dd>
		<dt><code>*SPHE</code></dt><dd>spherical coordinate system, generally used for SPS</dd>
	</dl>
)""");
const QString ChabaPlugin::_POINT_STRING = QObject::tr(R"""(<h3>Point</h3>
	<p>A point is defined by a name and its coordinate. It can have optional sigmas on its coordinates, and optional comments at the end.</p>)""");
const QString ChabaPlugin::_POINTREF_STRING = QObject::tr(R"""(<h3 style="color: blue;">Reference point</h3>
	<p>This point is used as an active or reference point. This is why the whole line is in bold, and it has the following marker:
	<img src=":/markers/reference" /></p>
	<p>For a point to be defined as an active (reference) point, it must be declared in both the active and the passive file.</p>
)""");
const QString ChabaPlugin::_POINTDEAC_STRING = QObject::tr(R"""(<h3 style="color: red;">Deactivated Point</h3>
	<p>A point is deactivated in the project.</p>)""");
const QString ChabaPlugin::_POINTINVALID_STRING = QObject::tr(R"""(<h3 style="color: red;">Invalid Point</h3>
	<p>Point is considered invalid either because its name repeats or the data defined for it is in wrong format.</p>)""");

ChabaPlugin::ChabaPlugin(QObject *parent) : QObject(parent), _pimpl(std::make_unique<_ChabaPlugin_pimpl>())
{
}

ChabaPlugin::~ChabaPlugin() = default;

// methods
QIcon ChabaPlugin::icon() const
{
	return QIcon(":/icons/chaba");
}

void ChabaPlugin::init()
{
	ClipboardManager::getClipboardManager().add(std::pair{new ChabaOutputLexer(), false}, std::pair{new ChabaPointLexerIO(), false});
	// Register entry points
	SPluginLoader::getPluginLoader().registerEntryPoint("plugin_lexers", {&EPplugin_lexers::staticMetaObject, this});
	logDebug() << "Plugin '" << name() << "' loaded";
}

void ChabaPlugin::terminate()
{
	ClipboardManager::getClipboardManager().erase(ChabaPointLexerIO().getMIMEType());
	logDebug() << "Plugin '" << name() << "' unloaded";
}

SConfigWidget *ChabaPlugin::configInterface(QWidget *parent)
{
	logDebug() << "loading ChabaPlugin::configInterface (ChabaConfig)";
	return new ChabaConfig(this, parent);
}

SGraphicalWidget *ChabaPlugin::graphicalInterface(QWidget *parent)
{
	logDebug() << "loading ChabaPlugin::graphicalInterface (ChabaGraphicalWidget)";
	auto *gui = new ChabaGraphicalWidget(this, parent);
	connect(gui, &QObject::destroyed, [this, gui]() -> void { _pimpl->projects.erase(gui); });
	_pimpl->projects.emplace(gui, ChabaProject{});
	return gui;
}

SLaunchObject *ChabaPlugin::launchInterface(QObject *parent)
{
	logDebug() << "loading ChabaPlugin::launchInterface (ChabaLauncherObject)";
	return new ChabaLauncherObject(this, parent);
}

QString ChabaPlugin::getExtensions() const noexcept
{
	return tr("Chaba (*.chaba)");
}

const QString &ChabaPlugin::_name()
{
	static const QString _sname = QString::fromStdString(getPluginName());
	return _sname;
}

const QString &ChabaPlugin::_baseMimetype()
{
	static const QString _sbaseMimetype = "application/vnd.cern.susoft.surveypad.plugin.chaba";
	return _sbaseMimetype;
}

const ShareablePointsList &ChabaPlugin::_activePointsTemplate()
{
	static const ShareablePointsList sspl = ([]() -> ShareablePointsList {
		ShareablePointsList spl("ACTIVE TEMPLATE");
		spl.getParams().coordsys = ShareableParams::ECoordSys::k3DCartesian;
		spl.getParams().extraInfos.addExtraInfo("coordinateSystem", "*OLOC");
		auto &frame = spl.getRootFrame();

		frame.setName("activeFrame");
		frame.add(new ShareablePoint{"ORIGIN",
			{
				0,
				0,
				0,
			},
			"Origin"});
		frame.add(new ShareablePoint{"pointX", {0, 0, 1}, "X axis"});
		frame.add(new ShareablePoint{"pointY", {0, 1, 0}, "Y axis"});
		frame.add(new ShareablePoint{"pointZ", {1, 0, 0, 0.1, 0.1, 0.5}, "Z axis"});
		auto &child = frame.addFrame();
		child.setName("childFrame");
		child.add(new ShareablePoint{"dummy1", {12, 13, 14, 1, 1, 1}});
		child.add(new ShareablePoint{"dummy2", {-12, -13, -14, 1, 1, 1}, "This point is desactivated", "", false, {{"DCUM", "DCUM"}, {"id", "ID"}}});
		return spl;
	})();

	return sspl;
}

const ShareablePointsList &ChabaPlugin::_passivePointsTemplate()
{
	static const ShareablePointsList sspl = ([]() -> ShareablePointsList {
		ShareablePointsList spl("PASSIVE TEMPLATE");
		spl.getParams().coordsys = ShareableParams::ECoordSys::k3DCartesian;
		spl.getParams().extraInfos.addExtraInfo("coordinateSystem", "*OLOC");
		auto &frame = spl.getRootFrame();

		frame.setName("passiveFrame");
		frame.add(new ShareablePoint{"ORIGIN", {0.5, 0.5, 0.5}});
		frame.add(new ShareablePoint{"pointX", {0.5, 0.5, 1.5}});
		frame.add(new ShareablePoint{"pointY", {0.5, 1.5, 0.5}});
		frame.add(new ShareablePoint{"pointZ", {1.5, 0.5, 0.5}});

		frame.add(new ShareablePoint{"point1", {20.699, 86.76, 52.139}});
		frame.add(new ShareablePoint{"point2", {87.692, 88.859, 19.764}});
		frame.add(new ShareablePoint{"point3", {21.943, 88.524, 54.163}});
		frame.add(new ShareablePoint{"point4", {54.045, 4.981, 60.093}});
		frame.add(new ShareablePoint{"point5", {16.546, 66.724, 48.982}});
		frame.add(new ShareablePoint{"point6", {74.009, 85.982, 24.991}});
		frame.add(new ShareablePoint{"point7", {39.307, 47.004, 51.617}});
		frame.add(new ShareablePoint{"point8", {31.686, 30.446, 97.337}});
		frame.add(new ShareablePoint{"point9", {62.224, 32.926, 44.784}});
		frame.add(new ShareablePoint{"point10", {39.778, 18.683, 35.617}});
		frame.add(new ShareablePoint{"point11", {37.355, 26.429, 40.934}});
		frame.add(new ShareablePoint{"point12", {64.642, 89.094, 73.177}});
		frame.add(new ShareablePoint{"point13", {53.689, 40.072, 73.312}});
		frame.add(new ShareablePoint{"point14", {43.613, 37.98, 34.219}});
		frame.add(new ShareablePoint{"point15", {90.651, 90.985, 23.522}});
		frame.add(new ShareablePoint{"point16", {98.166, 30.077, 91.51}});
		frame.add(new ShareablePoint{"point17", {42.219, 72.855, 89.929}});
		frame.add(new ShareablePoint{"point18", {23.843, 81.501, 16.441}});
		frame.add(new ShareablePoint{"point19", {56.738, 56.506, 90.611}});
		frame.add(new ShareablePoint{"point20", {89.699, 20.838, 78.284}});
		return spl;
	})();

	return sspl;
}

const ChabaProject *ChabaPlugin::_getProject(const QObject *id) const
{
	auto *root = findAncestor(id);
	if (!root || _pimpl->projects.find(root) == std::end(_pimpl->projects))
		return nullptr;
	return &_pimpl->projects.at(root);
}
