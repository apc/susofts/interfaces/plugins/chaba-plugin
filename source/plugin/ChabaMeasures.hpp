/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAMEASURES_HPP
#define CHABAMEASURES_HPP

#include <algorithm>
#include <functional>
#include <unordered_map>

#include <QHash>
#include <QString>

#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
namespace std
{
template<>
struct hash<QString>
{
	std::size_t operator()(const QString &s) const noexcept { return (size_t)qHash(s); }
};
} // namespace std
#endif

struct ChabaMeasures
{
	struct Value
	{
		/** position in the editor */
		int position = -1;
		/** real name */
		QString name;
		/** representation (with options...) */
		QString representation;
		/** Referential */
		QString type;
		/** default target for instrument, PDOR for points, linked instrument for targets */
		QString others;
		/** comment before the current statement */
		QString headerComment;
	};

	using MeasureMap = std::unordered_map<int, Value>; // identifier to Value
	/** key is point name */
	MeasureMap points;

	void clearAll() noexcept { points.clear(); }
};

static inline int hashString(const std::string &txt)
{
	// data conversion loss, increased collision chance, but QScintilla requires int for indicator so there is no way around it
	const static std::hash<std::string> hasher;
	return static_cast<int>(hasher(txt));
}

static inline int hashString(const QString &txt)
{
	return hashString(txt.toStdString());
}

inline bool contains(const ChabaMeasures::MeasureMap &map, const QString &key)
{
	return map.find(hashString(key)) != std::cend(map);
}

inline bool contains(const ChabaMeasures::MeasureMap &map, const std::string &key)
{
	return contains(map, QString::fromStdString(key));
}

#endif // CHABAMEASURES_HPP
