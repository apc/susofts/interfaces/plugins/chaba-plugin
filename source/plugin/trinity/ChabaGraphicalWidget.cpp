#include "ChabaGraphicalWidget.hpp"

#include <QDateTime>
#include <QFileInfo>
#include <QMessageBox>
#include <QTabWidget>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <interface/SPluginLoader.hpp>
#include <trinity/ChabaConfig.hpp>
#include <utils/FileDialog.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "ChabaPlugin.hpp"
#include "tabs/inputs/ChabaPointEditor.hpp"
#include "tabs/outputs/ChabaCooText.hpp"
#include "tabs/outputs/ChabaOutputText.hpp"
#include "tabs/project/ChabaProjectEditor.hpp"

class ChabaGraphicalWidget::_ChabaGraphicalWidget_pimpl
{
public:
	/** owning plugin */
	ChabaPlugin *plugin = nullptr;
	/** project file */
	ChabaProjectEditor *projectFileEditor = nullptr;
	/** active (reference) points */
	ChabaPointEditor *activePointsEditor = nullptr;
	/** passive points */
	ChabaPointEditor *passivePointsEditor = nullptr;
	/** output file */
	ChabaOutputText *outputEditor = nullptr;
	/** punch file */
	ChabaCooText *punchEditor = nullptr;
	/** coo file */
	ChabaCooText *cooEditor = nullptr;
	/** log file */
	TextEditorWidget *logEditor = nullptr;

	// Entry-point related
	/** Action coming from `plugin_uploadresults` entry point that on click should redirect to geode-plugin with proper upload website. */
	QAction *actionGeodeUpload = nullptr;
};

ChabaGraphicalWidget::ChabaGraphicalWidget(SPluginInterface *owner, QWidget *parent) :
	TabEditorScriptWidget(owner, parent), _pimpl(std::make_unique<_ChabaGraphicalWidget_pimpl>())
{
	sameDescriptionMode(false);
	_pimpl->plugin = dynamic_cast<ChabaPlugin *>(owner);
	setupEditors();
	setupActions();
	setupEntryPoints();

	connect(tab(), &QTabWidget::currentChanged, this, &ChabaGraphicalWidget::tabHasChanged);
	setupScripts();
}

ChabaGraphicalWidget::~ChabaGraphicalWidget() = default;

QString ChabaGraphicalWidget::getPath() const
{
	return _pimpl->projectFileEditor->getPath();
}

void ChabaGraphicalWidget::aboutTorun()
{
	TabEditorWidget::aboutTorun();
	removeOutputs();
}

void ChabaGraphicalWidget::runFinished(bool status)
{
	TabEditorWidget::runFinished(status);
	openOutputs(*_pimpl->plugin->_getProject(this), !status);
	// After it finishes the execution it shows the tab "Result" (number 3)
	if (tab()->count() > 3)
		tab()->setCurrentIndex(3);
}

void ChabaGraphicalWidget::clearContent()
{
	TabEditorWidget::clearContent();
	if (_pimpl->plugin && _pimpl->plugin->_getProject(this))
	{
		_pimpl->plugin->_getProject(this)->activePoints.clear();
		_pimpl->plugin->_getProject(this)->passivePoints.clear();
	}
}

void ChabaGraphicalWidget::importActive()
{
	QString filename = getOpenFileName(this, tr("Open file"), "", tr("Active (*.act);;Normal text file (*.txt);;All (*)"));
	if (filename.isEmpty())
		return;
	if (!_pimpl->activePointsEditor->open(filename))
	{
		logCritical() << "Can't open a file or the contents of the file are corrupted" << filename;
		return;
	}
	_pimpl->activePointsEditor->isModified(true);
	emit hasChanged();
	tab()->setCurrentWidget(_pimpl->activePointsEditor);
}

void ChabaGraphicalWidget::importPassive()
{
	QString filename = getOpenFileName(this, tr("Open file"), "", tr("Pasive (*.pas);;Coo (*.coo);;Normal text file (*.txt);;All (*)"));
	if (filename.isEmpty())
		return;
	if (!_pimpl->passivePointsEditor->open(filename))
	{
		logCritical() << "Can't open a file or the contents of the file are corrupted" << filename;
		return;
	}
	_pimpl->activePointsEditor->isModified(true);
	emit hasChanged();
	tab()->setCurrentWidget(_pimpl->passivePointsEditor);
}

bool ChabaGraphicalWidget::_save(const QString &path)
{
	ChabaProject &project = *_pimpl->plugin->_getProject(this);
	if (project.projectPath.empty()) // new save
	{
		if (!updateProjectPaths(QFileInfo(path).completeBaseName().toStdString()))
			return false;
	}
	else if (project.projectPath != path.toStdString()) // save As
	{
		project.projectPath = ""; // empty the path
		return save(path);
	}

	bool value = runWithNoSignals<bool>([this, &path, &project]() -> bool {
		// project file
		if (!_pimpl->projectFileEditor->save(path))
		{
			logCritical() << "Can't save to file" << path;
			return false;
		}
		try
		{
			*_pimpl->plugin->_getProject(this) = _pimpl->projectFileEditor->getProject();
		}
		catch (const SPIOException &e)
		{
			logCritical() << "Error: Can't save all the files because the project file has some errors:" << path << "\n\tat offset" << e.offset();
			return false;
		}
		tab()->setTabToolTip(0, path);
		project.projectPath = path.toStdString();

		// If automatic overwrite unchecked and active and passive files exist
		auto config = SettingsManager::settings().settings<ChabaConfigObject>(ChabaPlugin::_name());
		if (!config.disableCheckOverwrite)
		{
			QFileInfo active = getFullPath(project.activeFile);
			QFileInfo passive = getFullPath(project.passiveFile);
			if ((active.exists() && active != QFileInfo(_pimpl->activePointsEditor->getPath())) || (passive.exists() && passive != QFileInfo(_pimpl->passivePointsEditor->getPath())))
			{
				auto answer = QMessageBox::question(this, tr("Overwriting files"),
					tr("Warning: the active or passive file will overwrite an existing file in the directory. Do you want to continue?"
					   "\nRemember that you can always change the name of active and passive files in the `Project` tab."),
					QMessageBox::Yes | QMessageBox::No);
				if (answer == QMessageBox::No)
					return false;
			}
		}

		QString file;
		// active file
		file = getFullPath(project.activeFile);
		if (!_pimpl->activePointsEditor->save(file))
		{
			logCritical() << "Can't save to file" << file;
			return false;
		}
		tab()->setTabToolTip(1, file);
		// passive file
		file = getFullPath(project.passiveFile);
		if (!_pimpl->passivePointsEditor->save(file))
		{
			logCritical() << "Can't save to file" << file;
			return false;
		}
		tab()->setTabToolTip(2, file);
		return true;
	});
	updateWindowTitle();
	return value;
}

bool ChabaGraphicalWidget::_open(const QString &path)
{
	return runWithNoSignals<bool>([this, path]() -> bool {
		QFileInfo fi(path);
		QString file;
		// project file
		if (!_pimpl->projectFileEditor->open(path))
		{
			logCritical() << "Can't open file" << path;
			return false;
		}
		tab()->setTabToolTip(0, path);
		ChabaProject project;
		try
		{
			project = _pimpl->projectFileEditor->getProject();
		}
		catch (const std::exception &e)
		{
			logCritical() << e.what() << "\nCan't properly read the project file" << path;
			return false;
		}
		project.projectPath = path.toStdString();
		*_pimpl->plugin->_getProject(this) = project;
		// active file
		if (project.activeFile.empty())
			project.activeFile = fi.baseName().toStdString() + ".act";
		file = getFullPath(project.activeFile);
		if (!_pimpl->activePointsEditor->open(file))
		{
			logCritical() << "Can't open file" << file;
			return false;
		}
		tab()->setTabToolTip(1, file);
		// passive file
		if (project.passiveFile.empty())
			project.passiveFile = fi.baseName().toStdString() + ".pas";
		file = getFullPath(project.passiveFile);
		if (!_pimpl->passivePointsEditor->open(file))
		{
			logCritical() << "Can't open file" << file;
			return false;
		}
		tab()->setTabToolTip(2, file);
		_pimpl->passivePointsEditor->updateContent();
		_pimpl->activePointsEditor->updateContent();
		_pimpl->activePointsEditor->isModified(false);
		_pimpl->passivePointsEditor->isModified(false);

		// outputs
		openOutputs(project);

		updateContent();
		updateWindowTitle();
		return true;
	});
}

void ChabaGraphicalWidget::_newEmpty()
{
	runWithNoSignals<void>([this]() -> void {
		// project file
		_pimpl->projectFileEditor->newEmpty();
		*_pimpl->plugin->_getProject(this) = _pimpl->projectFileEditor->getProject();
		tab()->setTabToolTip(0, "");
		// active file
		_pimpl->activePointsEditor->newEmpty();
		tab()->setTabToolTip(1, "");
		_pimpl->activePointsEditor->updateContent();
		// passive file
		_pimpl->passivePointsEditor->newEmpty();
		tab()->setTabToolTip(2, "");
		_pimpl->passivePointsEditor->updateContent();

		// outputs
		removeOutputs();

		updateContent();
	});
	updateWindowTitle();
}

void ChabaGraphicalWidget::openOutputs(const ChabaProject &project, bool onlyLog)
{
	QFileInfo fi(QString::fromStdString(project.projectPath));

	auto openfile = [this, &fi](TextEditorWidget *widget, QString file) -> bool {
		if (!file.isEmpty())
		{
			if (fi.lastModified() < QFileInfo(file).lastModified() && widget->open(file))
			{
				widget->enablefileWatcher(true);
				widget->editor()->setReadOnly(true);
				setBackground(widget->editor(), getThemeColor(QColor(240, 240, 240)));
				tab()->addTab(widget, widget->windowTitle());
				tab()->addTab(widget, widget->windowTitle());
				tab()->setTabToolTip(tab()->indexOf(widget), file);
				return true;
			}
			else
				logDebug() << "Can't open file" << file;
		}
		delete widget;
		return false;
	};

	removeOutputs();
	if (!onlyLog)
	{
		// result
		_pimpl->outputEditor = new ChabaOutputText(_pimpl->plugin, this);
		connect(_pimpl->outputEditor->editor(), &TextEditor::globalIndicatorClicked, this, &ChabaGraphicalWidget::globalIndicatorClicked);
		_pimpl->outputEditor->chabaMeasures(_pimpl->activePointsEditor->chabaMeasures());
		_pimpl->outputEditor->setWindowTitle(tr("Result"));
		if (!openfile(_pimpl->outputEditor, getFullPath(project.outFile.empty() ? fi.completeBaseName().toStdString() + ".out" : project.outFile)))
			_pimpl->outputEditor = nullptr;
		// coo
		_pimpl->cooEditor = new ChabaCooText(_pimpl->plugin, this);
		_pimpl->cooEditor->setWindowTitle(tr("Coo"));
		if (!openfile(_pimpl->cooEditor, getFullPath(project.cooFile)))
			_pimpl->cooEditor = nullptr;
		if (_pimpl->cooEditor && _pimpl->actionGeodeUpload)
			_pimpl->cooEditor->toolbar()->addAction(_pimpl->actionGeodeUpload);

		// punch
		_pimpl->punchEditor = new ChabaCooText(_pimpl->plugin, this);
		_pimpl->punchEditor->setWindowTitle(tr("Punch"));
		if (!openfile(_pimpl->punchEditor, getFullPath(project.punchFile)))
			_pimpl->punchEditor = nullptr;
	}
	if (_pimpl->actionGeodeUpload)
		_pimpl->actionGeodeUpload->setEnabled(_pimpl->cooEditor);

	// log
	try
	{
		_pimpl->logEditor = new TextEditorWidget(_pimpl->plugin, this);
		_pimpl->logEditor->setWindowTitle(tr("Logs"));
		if (!openfile(_pimpl->logEditor, getFullPath(fi.completeBaseName().toStdString() + "Log.log")))
			_pimpl->logEditor = nullptr;
	}
	catch (...)
	{
	}

	restoreTabsAfterRun();
}

void ChabaGraphicalWidget::removeOutputs()
{
	QWidget *w = nullptr;

	tab()->setCurrentIndex(0);
	while (w = tab()->widget(3))
		delete w;
	_pimpl->outputEditor = nullptr;
	_pimpl->cooEditor = nullptr;
	_pimpl->punchEditor = nullptr;
	_pimpl->logEditor = nullptr;
}

QString ChabaGraphicalWidget::getFullPath(const std::string &file)
{
	return QFileInfo(QString::fromStdString(_pimpl->plugin->_getProject(this)->projectPath)).canonicalPath() + '/' + QString::fromStdString(file);
}

void ChabaGraphicalWidget::updateWindowTitle()
{
	QString title = QFileInfo(QString::fromStdString(_pimpl->plugin->_getProject(this)->projectPath)).baseName();
	if (title.isEmpty())
		title = tr("New Project");
	setWindowTitle(title);
}

bool ChabaGraphicalWidget::updateProjectPaths(const std::string &filename)
{
	if (filename.find_first_of(' ') != std::string::npos) // if contains spaces
	{
		QMessageBox::information(this, "Non-standard title", "Please provide a name without spaces for project files");
		return false;
	}
	ChabaProject &project = *_pimpl->plugin->_getProject(this);
	project.title = filename;
	project.passiveFile = project.title + ".pas";
	project.activeFile = project.title + ".act";
	project.cooFile = project.title + ".coo";
	project.punchFile = project.title + ".pun";
	project.outFile = project.title + ".out";

	_pimpl->projectFileEditor->setProject(project);
	return true;
}

void ChabaGraphicalWidget::setupEditors()
{
	// inputs
	// project
	_pimpl->projectFileEditor = new ChabaProjectEditor(_pimpl->plugin, this);
	_pimpl->projectFileEditor->setWindowTitle(tr("Project"));
	connect(_pimpl->projectFileEditor, &SGraphicalWidget::hasChanged, this, &ChabaGraphicalWidget::inputHasChanged);
	tab()->addTab(_pimpl->projectFileEditor, _pimpl->projectFileEditor->windowTitle());
	// active
	_pimpl->activePointsEditor = new ChabaPointEditor(_pimpl->plugin, true, this);
	_pimpl->activePointsEditor->setWindowTitle(tr("Active (reference) points"));
	connect(_pimpl->activePointsEditor, &SGraphicalWidget::hasChanged, this, &ChabaGraphicalWidget::inputHasChanged);
	tab()->addTab(_pimpl->activePointsEditor, _pimpl->activePointsEditor->windowTitle());
	// passive
	_pimpl->passivePointsEditor = new ChabaPointEditor(_pimpl->plugin, false, this);
	_pimpl->passivePointsEditor->setWindowTitle(tr("Passive points"));
	connect(_pimpl->passivePointsEditor, &SGraphicalWidget::hasChanged, this, &ChabaGraphicalWidget::inputHasChanged);
	tab()->addTab(_pimpl->passivePointsEditor, _pimpl->passivePointsEditor->windowTitle());
}

void ChabaGraphicalWidget::setupActions()
{
	auto *actionImportActive = new QAction(tr("Import active points"), this);
	actionImportActive->setToolTip(tr("Import active points from file..."));
	actionImportActive->setStatusTip(tr("Import active points from file..."));
	connect(actionImportActive, &QAction::triggered, this, &ChabaGraphicalWidget::importActive);
	auto *actionImportPassive = new QAction(tr("Import passive points"), this);
	actionImportPassive->setToolTip(tr("Import passive points from file..."));
	actionImportPassive->setStatusTip(tr("Import passive points from file..."));
	connect(actionImportPassive, &QAction::triggered, this, &ChabaGraphicalWidget::importPassive);

	addAction(Menus::plugin, actionImportActive);
	addAction(Menus::plugin, actionImportPassive);
}

void ChabaGraphicalWidget::setupEntryPoints()
{
	// plugin_uploadresults
	// Check for GEODE upload results entry point
	auto EPuploadresults = SPluginLoader::getPluginLoader().getEntryPoints("plugin_uploadresults");
	if (EPuploadresults.empty())
		return;
	// Vector should contain just one element (as Geode is the only one that registers under this entrypoint name)
	auto instanceEP = std::unique_ptr<QObject>(EPuploadresults[0].meta->newInstance());
	if (!instanceEP)
		return;

	std::function<void(const QString &, const QString &lgc)> uploadFunction;
	bool callGetAction = QMetaObject::invokeMethod(instanceEP.get(), "getUploadAction", Q_RETURN_ARG(QAction *, _pimpl->actionGeodeUpload));
	bool callGetFunction = QMetaObject::invokeMethod(instanceEP.get(), "getUploadFunction", Q_RETURN_ARG(std::function<void(const QString &, const QString &)>, uploadFunction));
	if (callGetAction && _pimpl->actionGeodeUpload && callGetFunction && uploadFunction)
	{
		addAction(Menus::plugin, _pimpl->actionGeodeUpload);
		// Default argument is not allowed - so we send an empty path for .lgc file
		connect(_pimpl->actionGeodeUpload, &QAction::triggered,
			[uploadFunction, this]() -> void { uploadFunction(getFullPath(_pimpl->projectFileEditor->getProject().cooFile), ""); });
	}
}

void ChabaGraphicalWidget::globalIndicatorClicked(const QString &text, int position)
{
	tab()->setCurrentIndex(1); // active
	_pimpl->activePointsEditor->globalIndicatorClicked(text, position);
}

void ChabaGraphicalWidget::tabHasChanged(int pos)
{
	if (pos != 1 && pos != 2)
		return;

	auto *newtab = qobject_cast<SGraphicalWidget *>(tab()->widget(pos));
	if (newtab)
		newtab->updateContent();
}

void ChabaGraphicalWidget::inputHasChanged()
{
	if (!isModified())
		return;
	if (_pimpl->outputEditor)
		_pimpl->outputEditor->inputHasChanged(true);
	if (_pimpl->punchEditor)
		_pimpl->punchEditor->inputHasChanged(true);
	if (_pimpl->cooEditor)
		_pimpl->cooEditor->inputHasChanged(true);
	if (_pimpl->logEditor)
		_pimpl->logEditor->inputHasChanged(true);
}
