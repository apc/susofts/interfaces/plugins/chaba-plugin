#include "ChabaConfig.hpp"
#include "ui_ChabaConfig.h"

#include <QDir>
#include <QFileDialog>
#include <QSettings>

#include "ChabaPlugin.hpp"
#include "Version.hpp"

bool ChabaConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const ChabaConfigObject &oc = static_cast<const ChabaConfigObject &>(o);
	return chabaPath == oc.chabaPath && color == oc.color && showTextEditors == oc.showTextEditors && showGUIEditors == oc.showGUIEditors
		&& disableCheckOverwrite == oc.disableCheckOverwrite;
}

void ChabaConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(ChabaPlugin::_name());

	settings.setValue("chabaPath_v" + chabaVersion, chabaPath);
	settings.setValue("color", color);
	settings.setValue("showTextEditors", showTextEditors);
	settings.setValue("showGUIEditors", showGUIEditors);
	settings.setValue("checkOverwrite", disableCheckOverwrite);

	settings.endGroup();
}

void ChabaConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(ChabaPlugin::_name());

	chabaPath = settings.value("chabaPath_v" + chabaVersion, "C:/Program Files/SUSoft/ChaBa/" + chabaVersion + "/CHABA.exe").toString();
	color = settings.value("color", true).toBool();
	showTextEditors = settings.value("showTextEditors", true).toBool();
	showGUIEditors = settings.value("showGUIEditors", true).toBool();
	disableCheckOverwrite = settings.value("checkOverwrite", false).toBool();

	settings.endGroup();
}

ChabaConfig::ChabaConfig(SPluginInterface *owner, QWidget *parent) : SConfigWidget(owner, parent), ui(std::make_unique<Ui::ChabaConfig>())
{
	ui->setupUi(this);
	ui->lblVersion->setText(ui->lblVersion->text().arg(QString::fromStdString(getVersion())));
}

ChabaConfig::~ChabaConfig() = default;

SConfigObject *ChabaConfig::config() const
{
	auto *tmp = new ChabaConfigObject();
	tmp->chabaPath = QDir::fromNativeSeparators(ui->lineEditChabaPath->text());
	tmp->color = ui->colorCheck->isChecked();
	tmp->showTextEditors = ui->checkText->isChecked() || ui->checkBoth->isChecked();
	tmp->showGUIEditors = ui->checkGui->isChecked() || ui->checkBoth->isChecked();
	tmp->disableCheckOverwrite = ui->checkOverwrite->isChecked();
	return tmp;
}

void ChabaConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const ChabaConfigObject *>(conf);
	if (config)
	{
		ui->lineEditChabaPath->setText(QDir::toNativeSeparators(config->chabaPath));
		ui->colorCheck->setChecked(config->color);
		ui->checkText->setChecked(config->showTextEditors);
		ui->checkGui->setChecked(config->showGUIEditors);
		ui->checkBoth->setChecked(config->showTextEditors && config->showGUIEditors);
		ui->checkOverwrite->setChecked(config->disableCheckOverwrite);
	}
}

void ChabaConfig::reset()
{
	ChabaConfigObject config;
	config.read();
	setConfig(&config);
}

void ChabaConfig::restoreDefaults()
{
	ChabaConfigObject config;
	setConfig(&config);
}

void ChabaConfig::selectChabaPath()
{
	QString filename = QFileDialog::getOpenFileName(this, tr("Chaba executable file"), ui->lineEditChabaPath->text(), tr("CHABA.exe (CHABA.exe);;Executable (*.exe);;Any (*.*)"));
	if (!filename.isEmpty())
		ui->lineEditChabaPath->setText(QDir::toNativeSeparators(filename));
}
