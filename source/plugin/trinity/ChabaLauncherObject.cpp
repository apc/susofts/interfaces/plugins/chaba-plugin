#include "ChabaLauncherObject.hpp"

#include <QProcess>

#include <ShareablePoints/SPIOException.hpp>
#include <utils/SettingsManager.hpp>

#include "ChabaPlugin.hpp"
#include "trinity/ChabaConfig.hpp"

QString ChabaLauncherObject::exepath() const
{
	auto conf = SettingsManager::settings().settings<ChabaConfigObject>(ChabaPlugin::_name());
	return conf.chabaPath;
}

void ChabaLauncherObject::launch(const QString &file)
{
	if (file.isEmpty())
	{
		emit finished(false);
		throw SPIOException("Error: no Chaba project file given. Did you save your project?");
	}
	process().setProgram(exepath());
	process().setArguments({file});
	ProcessLauncherObject::launch(file);
}
