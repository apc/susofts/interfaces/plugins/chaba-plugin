/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABALAUNCHEROBJECT_HPP
#define CHABALAUNCHEROBJECT_HPP

#include <memory>

#include <interface/ProcessLauncherObject.hpp>

/**
 * Launcher for Chaba plugin.
 *
 * Launch Chaba process and wait for it to finish.
 */
class ChabaLauncherObject : public ProcessLauncherObject
{
	Q_OBJECT

public:
	ChabaLauncherObject(SPluginInterface *owner, QObject *parent = nullptr) : ProcessLauncherObject(owner, parent) {}
	virtual ~ChabaLauncherObject() override = default;

	// SLaunchObject
	virtual QString exepath() const override;

public slots:
	// ProcessLauncherObject
	virtual void launch(const QString &file) override;
};

#endif // CHABALAUNCHEROBJECT_HPP
