/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABACONFIG_HPP
#define CHABACONFIG_HPP

#include <memory>

#include <interface/SConfigInterface.hpp>

namespace Ui
{
class ChabaConfig;
}

struct ChabaConfigObject : public SConfigObject
{
	virtual ~ChabaConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	/** Path to Chaba executable */
	const QString chabaVersion = "5.2.0"; // Cannot be modified by a plugin user
	QString chabaPath = "C:/Program Files/SUSoft/ChaBa/" + chabaVersion + "/CHABA.exe";
	/** Tells if the plugin should have syntaxic coloration */
	bool color = true;
	/** Tells if text editors are shown */
	bool showTextEditors = true;
	/** Tells if GUI editors are shown */
	bool showGUIEditors = true;
	/** If true, active and passive files should be automatically overwritten */
	bool disableCheckOverwrite = false;
};

class ChabaConfig : public SConfigWidget
{
	Q_OBJECT

public:
	ChabaConfig(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~ChabaConfig();

	// SConfigInterface
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private slots:
	void selectChabaPath();

private:
	std::unique_ptr<Ui::ChabaConfig> ui;
};

#endif // CHABACONFIG_HPP
