/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CHABAPLUGIN_HPP
#define CHABAPLUGIN_HPP

#include <memory>

#include <interface/SPluginInterface.hpp>

class ChabaProject;

/**
 * Plugin interface for Chaba.
 *
 * This plugin is meant to be used with SurveyPad.
 *
 * This class implements the plugin interface for SurveyPad.
 */
class ChabaPlugin : public QObject, public SPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID SPluginInterfaceIID)
	Q_INTERFACES(SPluginInterface)

public:
	ChabaPlugin(QObject *parent = nullptr);
	virtual ~ChabaPlugin() override;

	virtual const QString &name() const noexcept override { return ChabaPlugin::_name(); }
	virtual QIcon icon() const override;

	virtual void init() override;
	virtual void terminate() override;

	virtual bool hasConfigInterface() const noexcept override { return true; }
	virtual SConfigWidget *configInterface(QWidget *parent = nullptr) override;
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget *parent = nullptr) override;
	virtual bool hasLaunchInterface() const noexcept override { return true; }
	virtual SLaunchObject *launchInterface(QObject *parent = nullptr) override;

	virtual QString getExtensions() const noexcept override;
	virtual bool isMonoInstance() const noexcept override { return false; }

	// not exposed methods
public:
	static const QString _HELP_STRING;
	static const QString _REFERENTIAL_STRING;
	static const QString _POINT_STRING;
	static const QString _POINTREF_STRING;
	static const QString _POINTDEAC_STRING;
	static const QString _POINTINVALID_STRING;

	/** @return the name of the plugin */
	static const QString &_name();
	/** @return the base mimetype usde */
	static const QString &_baseMimetype();
	/** @return a ShareablePointsList to use as an example */
	static const ShareablePointsList &_activePointsTemplate();
	/** @return a ShareablePointsList to use as an example */
	static const ShareablePointsList &_passivePointsTemplate();

	// variable
	/**
	 * @return the ChabaProject associated with this QWidget tree, or nullptr
	 *
	 * If the widget has an ancestor which is a SGraphicalWidget created by this plugin, it returns the associated ChabaProject.
	 */
	const ChabaProject *_getProject(const QObject *id) const;
	ChabaProject *_getProject(const QObject *id) { return const_cast<ChabaProject *>(static_cast<const ChabaPlugin *>(this)->_getProject(id)); }

private:
	/** pimpl */
	class _ChabaPlugin_pimpl;
	std::unique_ptr<_ChabaPlugin_pimpl> _pimpl;
};

#endif // CHABAPLUGIN_HPP
