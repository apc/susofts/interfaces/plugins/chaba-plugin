#include "EPplugin_lexers.hpp"

#include <interface/SPluginInterface.hpp>

#include "tabs/inputs/ChabaPointLexerIO.hpp"
#include "tabs/outputs/ChabaOutputLexer.hpp"
#include "tabs/project/ChabaProjectLexer.hpp"

QList<QString> EPplugin_lexers::getNames()
{
	return {ChabaPointLexerIO().language() , ChabaOutputLexer().language(), ChabaProjectLexer().language()}; // Chaba Active or Passive, Chaba Out file, Chaba Project
}

QList<std::function<QsciLexer *(QWidget *)>> EPplugin_lexers::getLexers()
{
	return {[](QWidget *parent) -> QsciLexer * { return new ChabaPointLexerIO(parent); }, [](QWidget *parent) -> QsciLexer * { return new ChabaOutputLexer(parent); },
		[](QWidget *parent) -> QsciLexer * { return new ChabaProjectLexer(parent); }};
}
