#ifndef EPLEXER_HPP
#define EPLEXER_HPP

#include <functional>

#include <QList>
#include <QObject>

class QsciLexer;

/**
 * Entry point class for "plugin_lexers" entry point.
 *
 * This class provides chaba-plugin lexers to be used externally.
 * The class (and all the others using "plugin_lexers" entry point) should expose the lexers using getLexers() method and their names with getNames() method.
 * The lexers and their names should be listed in alphabetical order.
 */
class EPplugin_lexers : public QObject
{
	Q_OBJECT

public:
	Q_INVOKABLE EPplugin_lexers(QObject *parent = nullptr) : QObject(parent) {}

	/** @return list of names describing lexers. */
	Q_INVOKABLE QList<QString> getNames();
	/** @return list of std::functions for lexer constructor methods. */
	Q_INVOKABLE QList<std::function<QsciLexer *(QWidget *)>> getLexers();
};

#endif // EPLEXER_HPP
