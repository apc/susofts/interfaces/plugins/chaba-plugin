License
=======

Some images used in the resources are under the LGPL license. They all come from <https://github.com/KDE/breeze-icons>. Check the file [LGPL.LIB](./LGPL.LIB) for more information about the LGPL license.
