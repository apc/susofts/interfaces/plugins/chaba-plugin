#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <tabs/inputs/ChabaPointLexerIO.hpp>

namespace tut
{
struct chabapointlexeriodata
{
};

typedef test_group<chabapointlexeriodata> tg;
tg plugin_chabapointlexerio_test_group("Test ChabaPointLexerIO class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("ChabaPointLexerIO: Test of base class");

	ChabaPointLexerIO chio;
	ensure_equals(chio.getMIMEType(), "application/vnd.cern.susoft.surveypad.plugin.chaba.pointRAW");
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ChabaPointLexerIO: Test of reading bad input");

	// emtpy
	ChabaPointLexerIO chio;
	ensure_THROW(chio.read(""), SPIOException);

	// garbage
	ensure_THROW(chio.read("lol"), SPIOException);

	// bad coordinate system
	ensure_THROW(chio.read("*LOL"), SPIOException);

	// good start
	ensure_THROW(chio.read("*OLOC\nLOL LOL LOL"), SPIOException);
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("ChabaPointLexerIO: Test of reading referentials");

	{
		// Referential provided
		ChabaPointLexerIO chio;
		ensure(chio.read("*RS2K\npointX 0.00000 0.00000 1.00000").getParams().extraInfos.at("coordinateSystem"), "*RS2K");
	}

	{
		// No referential provided
		ChabaPointLexerIO chio;
		ensure(chio.read("pointX 0.00000 0.00000 1.00000").getParams().extraInfos.at("coordinateSystem"), "*OLOC");
	}

}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ChabaPointLexerIO: Test of position IO");

	const ShareablePosition position{12.5, 0.769, 9.12, 0.001, 0.00001, 5.5};

	ChabaPointLexerIO chio;
	ShareablePosition loaded = chio.readPosition(chio.write(position));
	ensure(position == loaded);
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("ChabaPointLexerIO: Test of extraInfos IO");

	ensure(ChabaPointLexerIO().readExtraInfos("") == ShareableExtraInfos());
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("ChabaPointLexerIO: Test of point IO");

	const ShareablePoint point{"name", {12.5, 0.769, 9.12, 0.001, 0.00001, 5.5}, "inline comment", "", false, {{"DCUM", "dcum"}, {"id", "id"}}};

	ChabaPointLexerIO chio;
	ShareablePoint loaded = chio.readPoint(chio.write(point));
	ensure(point == loaded);

	// partial
	chio.exportFieldsPoint.removeField("extraInfos");
	auto tmp = point;
	tmp.extraInfos.clear();
	tmp.inlineComment.clear();
	loaded = chio.readPoint(chio.write(point));
	ensure(tmp == loaded);
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("ChabaPointLexerIO: Test of params IO");

	const ShareableParams params{5, ShareableParams::ECoordSys::k2DPlusH, {{"coordinateSystem", "*SPHE"}}};

	ChabaPointLexerIO chio;
	ShareableParams loaded = chio.readParams(chio.write(params));
	ensure(params == loaded);
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("ChabaPointLexerIO: Test of frame IO");

	// we create a tree like this (leaves have "*" prefix):
	// root ______ *p0
	//      \_____ *p1
	//       \____ child1 ______ *p2
	//        \___ child2 ______ *p3
	//         \__ child3 ______ *p4
	//                    \_____ *p5
	//                     \____ grandchild ______ *p6

	auto params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params);

	frame.addPoint().name = "p0";
	frame.addPoint().name = "p1";
	frame.addFrame().addPoint().name = "p2";
	frame.addFrame().addPoint().name = "p3";
	auto &child3 = frame.addFrame();
	child3.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "", false, {{"DCUM", "dcum"}, {"id", "id"}}});
	child3.addPoint().name = "p5";
	child3.addFrame().addPoint().name = "p6";

	ChabaPointLexerIO chio;
	ShareableFrame loaded = chio.readFrame(chio.write(frame));
	const auto &fpoints = frame.getAllPoints(), lpoints = loaded.getAllPoints();
	ensure(std::equal(std::cbegin(fpoints), std::cend(fpoints), std::cbegin(lpoints), [](const auto *a, const auto *b) -> bool { return *a == *b; }));
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("ChabaPointLexerIO: Test of points list IO");

	ShareablePointsList spl;
	spl.getParams().extraInfos.addExtraInfo("coordinateSystem", "*OLOC");
	spl.getParams().precision = 5;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "", false, {{"DCUM", "dcum"}, {"id", "id"}}});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	ChabaPointLexerIO chio;
	ShareablePointsList loaded = chio.read(chio.write(spl));
	ensure(spl == loaded);
}

template<>
template<>
void testobject::test<16>()
{
	set_test_name("ChabaPointLexerIO: Test read and write files");

	ShareablePointsList spl;
	spl.getParams().extraInfos.addExtraInfo("coordinateSystem", "*OLOC");
	spl.getParams().precision = 5;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "", false, {{"DCUM", "dcum"}, {"id", "id"}}});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	ChabaPointLexerIO chio;
	IShareablePointsListIO::writeFile("splistchabaTest.act", chio.write(spl));
	auto loaded = chio.read(IShareablePointsListIO::readFile("splistchabaTest.act"));
	ensure(spl == loaded);
	std::remove("splistchabaTest.act");
}
} // namespace tut
