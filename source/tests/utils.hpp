#ifndef TESTCHABAUTILS_HPP
#define TESTCHABAUTILS_HPP

#include <ostream>

inline std::ostream &operator<<(std::ostream &os, const ChabaProject &obj)
{
	os << obj.title;
	return os;
}

#endif // TESTCHABAUTILS_HPP
