#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <tabs/outputs/ChabaOutputLexer.hpp>

namespace tut
{
struct chabaoutputlexerdata
{
};

typedef test_group<chabaoutputlexerdata> tg;
tg plugin_chabaoutputlexer_test_group("Test ChabaOutputLexer class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("ChabaOutputLexer: Test of base class");

	ChabaOutputLexer chio;
	ensure_equals(chio.getMIMEType(), "application/vnd.cern.susoft.surveypad.plugin.chaba.outputpointRAW");
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ChabaOutputLexer: Test of reading bad input");

	ChabaOutputLexer chio;

	// garbage
	ensure_THROW(chio.read("lol"), SPIOException);

	// bad point
	ensure_THROW(chio.read("LOL\t6\tlol"), SPIOException);

	// good start
	ensure_THROW(chio.read("*OLOC\nLOL LOL LOL"), SPIOException);
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ChabaOutputLexer: Test of position IO");

	const ShareablePosition position{12.5, 0.769, 9.12};

	ChabaOutputLexer chio;
	ShareablePosition loaded = chio.readPosition("  12.5 0.769 9.12  ");
	ensure(position == loaded);
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("ChabaOutputLexer: Test of extraInfos IO");

	ensure(ChabaOutputLexer().readExtraInfos("") == ShareableExtraInfos());
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("ChabaOutputLexer: Test of point IO");

	ShareablePoint point{"name", {12.5, 0.769, 9.12}, "inline comment", "", false};

	ChabaOutputLexer chio;
	ShareablePoint loaded = chio.readPoint("!name 12.5 0.769 9.12 inline comment");
	ensure(point == loaded);
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("ChabaOutputLexer: Test of params IO");

	ensure(ChabaOutputLexer().readParams("") == ShareableParams());
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("ChabaOutputLexer: Test of frame IO");

	auto params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params);

	frame.add(new ShareablePoint{"p0", {7, 8, 9}, "inline", "", false});
	frame.addPoint().name = "p1";

	ChabaOutputLexer chio;
	ShareableFrame loaded = chio.readFrame(R"""(
			!p0 7 8 9 inline
			p1	0	0	0
		)""");
	const auto &fpoints = frame.getAllPoints(), lpoints = loaded.getAllPoints();
	ensure(std::equal(std::cbegin(fpoints), std::cend(fpoints), std::cbegin(lpoints), [](const auto *a, const auto *b) -> bool { return *a == *b; }));
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("ChabaOutputLexer: Test of points list IO");

	ShareablePointsList spl;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p0", {7, 8, 9}, "inline", "", false});
	frame.addPoint().name = "p1";

	ChabaOutputLexer chio;
	ShareablePointsList loaded = chio.read(R"""(
			!p0 7 8 9 inline
			p1	0	0	0
		)""");
	ensure(spl == loaded);
}
} // namespace tut
