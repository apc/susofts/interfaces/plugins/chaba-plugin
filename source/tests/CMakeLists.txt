set(test_name testsChabaPlugin)

set(TESTS_SOURCES
	main.cpp
	test_plugin.cpp
	test_ChabaOutputLexer.cpp
	test_ChabaPointLexerIO.cpp
	test_ChabaProject.cpp
	test_ChabaProjectLexer.cpp
)
set(TESTS_HEADERS
	utils.hpp
)

# As the plugin creates a DLL with just the plugin interface exposed,
# if we want to test inner classes, we have to integrate them statically here...
set(PLUGIN_SRCES "")
foreach(f ${PLUGIN_ALL_SRCES})
	if(NOT IS_ABSOLUTE ${f})
		set(f "../${f}")
	endif()
	list(APPEND PLUGIN_SRCES ${f})
endforeach()
add_executable(${test_name}
	# plugin
	${PLUGIN_SRCES}
	# tests
	${TESTS_HEADERS}
	${TESTS_SOURCES}
)
target_compile_definitions(${test_name} PRIVATE
	QSCINTILLA_DLL
	UIPlugins_IMPORT
	PROJECT_NAME="${PROJECT_NAME}"
)
target_include_directories(${test_name} PRIVATE
	${CMAKE_CURRENT_BINARY_DIR}
	${PROJECT_SOURCE_DIR}/plugin
	# TUT
	${TUT_INCLUDE_PATH}
	# SurveyLib
	${SURVEYLIB_ROOT}/source/Logs
	${SURVEYLIB_ROOT}/source/Plugins
	${SURVEYLIB_ROOT}/source/Tools/headers
	# SUGL
	${SUGL_ROOT}/source/UIPlugins
)
target_link_libraries(${test_name}
	# Qt
	Qt5::Core
	Qt5::Gui
	Qt5::Widgets
	# QScintilla
	qscintilla2_qt5
	# SurveyLib
	Plugins
	Tools
	# SUGL
	UIPlugins
)

get_target_property(QSCINTILLA_DLL_DEBUG qscintilla2_qt5 IMPORTED_LOCATION_DEBUG)	
get_target_property(QSCINTILLA_DLL qscintilla2_qt5 IMPORTED_LOCATION_RELEASE)
if(WIN32)
	deployqt(${test_name} "$<TARGET_FILE_DIR:${test_name}>" "$<$<CONFIG:debug>:${QSCINTILLA_DLL_DEBUG}>$<$<CONFIG:release>:${QSCINTILLA_DLL}>")
	deployqt(${test_name} "$<TARGET_FILE_DIR:${test_name}>" "$<TARGET_FILE:UIPlugins>")
	deployqt(${test_name} "$<TARGET_FILE_DIR:${test_name}>" "$<TARGET_FILE:${PROJECT_NAME}>")
endif()
post_build_copy_dlls(${test_name} "${QSCINTILLA_DLL_DEBUG}" "${QSCINTILLA_DLL}" "")
post_build_copy_dlls(${test_name} "$<TARGET_FILE:UIPlugins>" "$<TARGET_FILE:UIPlugins>" "" TRUE)
post_build_copy_dlls(${test_name} "$<TARGET_FILE:${PROJECT_NAME}>" "$<TARGET_FILE:${PROJECT_NAME}>" "plugins" TRUE)

add_test(
	NAME ${test_name}
	COMMAND $<TARGET_FILE_NAME:${test_name}>
	WORKING_DIRECTORY $<TARGET_FILE_DIR:${test_name}>
)

add_dependencies(${test_name} ${PROJECT_NAME}) # one test checks for the presence of the plugin .dll

# IDE
source_group("${test_name}" FILES ${TESTS_HEADERS} ${TESTS_SOURCES})
source_group("_plugin" FILES ${PLUGIN_SRCES})
