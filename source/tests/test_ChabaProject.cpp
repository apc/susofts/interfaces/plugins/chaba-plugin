#include <ostream>

#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ChabaProject.hpp>
#include <ShareablePoints/SPIOException.hpp>

#include "utils.hpp"

namespace tut
{
struct chabaprojectdata
{
};

typedef test_group<chabaprojectdata> tg;
tg plugin_chabaproject_test_group("Test ChabaProject class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("ChabaProject: Test of getters and setters");

	ChabaProject chpl;
	// default
	ensure_equals(chpl.title, "dummy");
	ensure_equals(chpl.precision, 5);
	ensure_equals(chpl.tolerance, 0.005);
	ensure_equals(chpl.maxIterations, 50);
	ensure(chpl.passiveFile.empty());
	ensure(chpl.activeFile.empty());
	ensure(chpl.cooFile.empty());
	ensure(chpl.punchFile.empty());
	ensure(chpl.outFile.empty());
	ensure_not(chpl.scale.variable);
	ensure_equals(chpl.scale.value, 1.);
	ensure(chpl.trax.variable);
	ensure_equals(chpl.trax.value, 0);
	ensure(chpl.tray.variable);
	ensure_equals(chpl.tray.value, 0);
	ensure(chpl.traz.variable);
	ensure_equals(chpl.traz.value, 0);
	ensure(chpl.rotx.variable);
	ensure_equals(chpl.rotx.value, 0);
	ensure(chpl.roty.variable);
	ensure_equals(chpl.roty.value, 0);
	ensure(chpl.rotz.variable);
	ensure_equals(chpl.rotz.value, 0);

	// we change the values
	chpl.title = "title";
	chpl.precision = 42;
	chpl.tolerance = 12.3;
	chpl.maxIterations = 15;
	chpl.passiveFile = "passiveFile";
	chpl.activeFile = "activeFile";
	chpl.cooFile = "cooFile";
	chpl.punchFile = "punchFile";
	chpl.outFile = "outFile";
	chpl.scale = {true, 0.5};
	chpl.trax = {false, 0.5};
	chpl.tray = {false, 0.5};
	chpl.traz = {false, 0.5};
	chpl.rotx = {false, 0.5};
	chpl.roty = {false, 0.5};
	chpl.rotz = {false, 0.5};
	// second test
	ensure_equals(chpl.title, "title");
	ensure_equals(chpl.precision, 42);
	ensure_equals(chpl.tolerance, 12.3);
	ensure_equals(chpl.maxIterations, 15);
	ensure_equals(chpl.passiveFile, "passiveFile");
	ensure_equals(chpl.activeFile, "activeFile");
	ensure_equals(chpl.cooFile, "cooFile");
	ensure_equals(chpl.punchFile, "punchFile");
	ensure_equals(chpl.outFile, "outFile");
	ensure(chpl.scale.variable);
	ensure_equals(chpl.scale.value, 0.5);
	ensure_not(chpl.trax.variable);
	ensure_equals(chpl.trax.value, 0.5);
	ensure_not(chpl.tray.variable);
	ensure_equals(chpl.tray.value, 0.5);
	ensure_not(chpl.traz.variable);
	ensure_equals(chpl.traz.value, 0.5);
	ensure_not(chpl.rotx.variable);
	ensure_equals(chpl.rotx.value, 0.5);
	ensure_not(chpl.roty.variable);
	ensure_equals(chpl.roty.value, 0.5);
	ensure_not(chpl.rotz.variable);
	ensure_equals(chpl.rotz.value, 0.5);
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ChabaProject: Test of equality operators");

	ensure_equals(ChabaProject(), ChabaProject());
	ensure_not(ChabaProject() != ChabaProject());

	// we change the values
	ChabaProject chpl;
	chpl.title = "title";
	chpl.precision = 42;
	chpl.tolerance = 12.3;
	chpl.maxIterations = 15;
	chpl.passiveFile = "passiveFile";
	chpl.activeFile = "activeFile";
	chpl.cooFile = "cooFile";
	chpl.punchFile = "punchFile";
	chpl.outFile = "outFile";
	chpl.scale = {true, 0.5};
	chpl.trax = {false, 0.5};
	chpl.tray = {false, 0.5};
	chpl.traz = {false, 0.5};
	chpl.rotx = {false, 0.5};
	chpl.roty = {false, 0.5};
	chpl.rotz = {false, 0.5};

	ensure_not(chpl == ChabaProject());
	ensure(chpl != ChabaProject());

	ensure_equals(chpl, chpl);
	ensure_not(chpl != chpl);
}
} // namespace tut
