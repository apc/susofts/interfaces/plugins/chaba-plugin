#include <ostream>

#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ChabaProject.hpp>
#include <ShareablePoints/IShareablePointsListIO.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <tabs/project/ChabaProjectLexer.hpp>

#include "utils.hpp"

namespace tut
{
struct chabaprojectlexerdata
{
};

typedef test_group<chabaprojectlexerdata> tg;
tg plugin_chabaprojectlexer_test_group("Test ChabaProjectLexer class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("ChabaProjectLexer: Test of reading bad input");

	ChabaProjectLexer chpl;

	// bad tolerance
	ensure_THROW(chpl.read("tolerance lol"), SPIOException);

	// bad translation
	ensure_THROW(chpl.read("TrX lol 0.01"), SPIOException);
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ChabaProjectLexer: Test of default values");

	ChabaProjectLexer chpl;
	ChabaProject project;
	project.activeFile = "active";
	project.passiveFile = "passive";
	project.outFile = "out";
	auto loaded = chpl.read(chpl.write(project));
	// default
	ensure_equals(loaded, project);
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("ChabaProjectLexer: Test of custom values");

	ChabaProject project, loaded;
	ChabaProjectLexer chpl;
	// we change the values
	project.title = "title";
	project.precision = 42;
	project.tolerance = 12.3;
	project.passiveFile = "passiveFile";
	project.activeFile = "activeFile";
	project.cooFile = "cooFile";
	project.punchFile = "punchFile";
	project.outFile = "outFile";
	project.scale = {true, 0.5};
	project.trax = {false, 0.5};
	project.tray = {false, 0.5};
	project.traz = {false, 0.5};
	project.rotx = {false, 0.5};
	project.roty = {false, 0.5};
	project.rotz = {false, 0.5};

	loaded = chpl.read(chpl.write(project));

	// test
	ensure_equals(loaded, project);
}

template<>
template<>
void testobject::test<16>()
{
	set_test_name("ChabaProjectLexer: Test read and write files");

	ChabaProject project, loaded;
	ChabaProjectLexer chpl;
	// we change the values
	project.title = "title";
	project.precision = 42;
	project.tolerance = 12.3;
	project.passiveFile = "passiveFile";
	project.activeFile = "activeFile";
	project.cooFile = "cooFile";
	project.punchFile = "punchFile";
	project.outFile = "outFile";
	project.scale = {true, 0.5};
	project.trax = {false, 0.5};
	project.tray = {false, 0.5};
	project.traz = {false, 0.5};
	project.rotx = {false, 0.5};
	project.roty = {false, 0.5};
	project.rotz = {false, 0.5};

	IShareablePointsListIO::writeFile("project.chaba", chpl.write(project));
	loaded = chpl.read(IShareablePointsListIO::readFile("project.chaba"));

	// test
	ensure_equals(loaded, project);

	std::remove("project.chaba");
}
} // namespace tut
